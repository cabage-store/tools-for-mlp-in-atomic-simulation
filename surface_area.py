#! usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import math
import numpy as np
from ase.io import read
from Converter.file_converter import get_symbols


dict = {'Cu': {'lattice_constant':3.6147,'radius':1.45},
        'Ag': {'lattice_constant':4.0853,'radius':1.65},
        'Au': {'lattice_constant':4.0782,'radius':1.65},
        'Ni': {'lattice_constant':3.524,'radius':1.49},
        'Pd':{'lattice_constant':3.8907,'radius':1.69},
        'Ru_hcp':{'lattice_constant':2.71,'radius':1.25},
        'Ru_fcc':{'lattice_constant':3.79,'radius':1.25},
        'Si':{'lattice_constant':3.8907,'radius':1.18}
                    }

def ele_percent(image, reverse=False):
    symbols = get_symbols(image, reverse)
    symbols_percent = []
    for i in range(len(symbols)):
        p = image.get_chemical_symbols().count(symbols[i]) / len(image)
        symbols_percent.append(p)
    return symbols, symbols_percent

# This method is suitable for the irregular clusters.
# The output is the radius from three method.
# Ref: 
def Cls_Radius_Calculator(image, symbols, ele_percent):
    R_Ni = 0
    for i in range(len(ele_percent)):
        R_percent = ele_percent[i] * dict[symbols[i]]['lattice_constant']
        R_Ni += R_percent
        #atomic mass matrics
        m_mol = image.get_masses()/6.02214076 * 10 ** -23
        #total mass of structure
        M_tot = np.sum(m_mol)
        #atomic position matrics
        R_cord = image.get_positions()
        new_m = np.reshape(m_mol,(len(image),1))
        #position matrics with weights of mass
        R_all = ((1/M_tot) * new_m * R_cord)
        # R_cm is the center of mass of image
        R_cm = sum(R_all)
        #R_dc distance between the center of mass and atoms
        R_dc = R_cord - R_cm
        R_1 = np.linalg.norm(R_dc, axis=1)
        R_now = np.square(R_1)
        
        #R_tot sum of the squares of the module lengths of each atom and the center of mass
        R_tot = np.sum(R_now)    
        R_g = math.sqrt((1/len(image)) * R_tot)
        # Method 1
        R_s = R_g * math.sqrt(5/3) + R_Ni
        #The norm (modulus length) of the difference between the coordinates of each atom and the center-of-mass atom
        R_method1 = R_s / 2
        # Method 2
        R_max = np.max(R_1)
        R_method2 = R_max / 2
        # Method 3
        ele_index = 0
        for i in range(len(R_cord)):
            if np.allclose(R_cord[i],R_cm):
                ele_index = i
        d_max = np.where(R_1==R_max)
        if np.size(d_max) > 1: 
            d_max = d_max[0][0]
        all_symbols = image.get_chemical_symbols()
        all_symbols = np.array(all_symbols)
        R_method3 = R_max/2 + (dict[all_symbols[ele_index].item()]['radius']+dict[all_symbols[d_max].item()]['radius'])/2
        return R_method1, R_method2, R_method3

def Surface_Area_Calculator(image, strtype):
    if strtype == 'IrrCluster':
        symbols, percent = ele_percent(image, reverse=True)
        R1, R2, R3 = Cls_Radius_Calculator(image, symbols, percent)
        S1, S2, S3 = [4 * np.pi * np.square(R) for R in [R1, R2, R3]]
        return S1, S2, S3
    elif strtype == 'Icosahedron':
        cm = image.get_center_of_mass()
        pos = image.get_positions()
        max_dis = np.max(np.linalg.norm(pos-cm, axis=1))
        R = max_dis / np.sin(np.pi*0.4)
        S = 5 * np.sqrt(3) * np.square(R)
        return S
    elif strtype == 'Tru_Octahedron':
        pos = image.get_positions()
        z_max = np.max(pos[:,2])
        max_atoms = pos[pos[:,2] == z_max]
        dis = np.linalg.norm(max_atoms[:, np.newaxis] - max_atoms, axis=-1)
        R = np.max(dis[dis != 0]) / np.sqrt(2)
        S = 6 + 12 * np.sqrt(3) * np.square(R)
        return S
    elif strtype == 'Reg_Octahedron':
        pos = image.get_positions()
        z_max = np.max(pos[:, 2])
        y_max = np.max(pos[:, 1])
        coord1 = pos[pos[:, 2] == z_max]
        coord2 = pos[pos[:, 1] == y_max]
        dis = np.linalg.norm(coord2 - coord1, axis=1)
        R = dis[0]
        S = 2 * np.sqrt(3) * np.square(R)
        return S
    # elif strtype == 'Decahedron':