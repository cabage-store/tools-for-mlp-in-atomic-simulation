'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-04-29 03:20:25
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-06-11 00:54:35
FilePath: /tools-for-mlp-in-atomic-simulation/pca-nep.py
'''
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from ase.io import read, write
from pynep.calculate import NEP

def PCAanalysis(apps, filename, potential, colors):
# Picture Style
    matplotlib.use('Agg')
    matplotlib.rcParams['xtick.direction'] = 'in'
    matplotlib.rcParams['ytick.direction'] = 'in' # in, out or inout
    matplotlib.rcParams['ytick.right'] = True
    matplotlib.rcParams['xtick.top'] = True
    fontsize = 16
    plt.rcParams['font.size'] = fontsize
    plt.rcParams['font.family'] = 'Times'
    plt.rcParams['font.weight'] = "heavy"
    plt.figure(figsize=(12, 8))
    if apps == 'n2p2':
        for i in range(len(filename)):
            des = np.loadtxt(filename)
            reducer = PCA(n_components=2)
            reducer.fit(des)
            proj = reducer.transform(des)
            plt.scatter(proj[:, 0], proj[:, 1], c=colors[i], label=f'data-{i}')
            plt.legend()
    if apps == 'NEP':
#main function 
        for i in range(len(filename)):
            image = read(filename[i], ':')
            calc = NEP(potential[i])
            print(calc)
            des = np.array([np.mean(calc.get_property('descriptor', i), axis=0) for i in image])
            reducer = PCA(n_components=2)
            reducer.fit(des)
            proj = reducer.transform(des)
            plt.scatter(proj[:, 0], proj[:, 1], c=colors[i], label=f'data-{i}')
            plt.legend()
    plt.savefig('pca.png', dpi=300)
if __name__ == '__main__':
    # a = ['niau.xyz','cuag.xyz','pdau.xyz']
    # b = ['niau.txt','cuag.txt','pdau.txt']
    # colors=['#ef5674', '#eebc59','#5891d5']
    # PCAanalysis(a,b,colors)
    a = ['train.xyz']
    b = ['nep.txt']
    colors=['#ef5674', '#eebc59','#5891d5']
    PCAanalysis('NEP', a,b,colors)
    