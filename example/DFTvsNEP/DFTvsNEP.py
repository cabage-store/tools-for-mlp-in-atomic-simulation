from pynep.calculate import NEP
from ase.io import read
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import random

matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.direction'] = 'in' # in, out or inout
matplotlib.rcParams['ytick.right'] = False
matplotlib.rcParams['xtick.top'] = False

# --------------------------------------

dft_file = '/Users/wx/Desktop/test.extxyz'
nep_file = '/Users/wx/Desktop/test.extxyz'

# --------------------------------------


s_dft = read(dft_file, ':')
s_nep = read(nep_file, ':')

calc = NEP("/Users/wx/Desktop/potential-rusi/nep0620-1.txt")
# print(calc)
e_dft, e_nep, f_dft, f_nep = [], [], [], []
for i, j in zip(s_dft, s_nep):
    j.set_calculator(calc)
    e_dft.append(i.get_potential_energy() / len(i))
    e_nep.append(j.get_potential_energy() / len(i))

    f_dft.append(i.get_forces().reshape(-1))
    f_nep.append(j.get_forces().reshape(-1))

e_dft = np.array(e_dft)
e_nep = np.array(e_nep)
f_dft = np.concatenate(f_dft)
f_nep = np.concatenate(f_nep)


def pltForce(fT, fP, npltdata=100000):

    if len(fT) > npltdata:
        tmpidex = random.sample(range(len(fT)), npltdata)
        y_test = fT[tmpidex]
        y_prediction = fP[tmpidex]
    else:
        y_test = fT
        y_prediction = fP
    
    ### calculate gaussian_kde
    from scipy.stats import gaussian_kde
    #import matplotlib.cm as cm
    xy = np.vstack([y_test, y_prediction])
    z = gaussian_kde(xy)(xy)
    idex = np.lexsort([z])
    fig,ax=plt.subplots()

    # plt.title("NEP forces vs DFT forces", fontsize=16)
    ax.set_aspect(1)
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    
    ymajorFormatter = ticker.FormatStrFormatter('%.1f') 
    xmajorFormatter = ticker.FormatStrFormatter('%.1f') 
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    
    ax.spines['bottom'].set_linewidth(2)
    ax.spines['left'].set_linewidth(2)
    ax.spines['right'].set_linewidth(2)
    ax.spines['top'].set_linewidth(2)

    ax.tick_params(labelsize=14)

    lowLimit = np.min([np.min(y_test), np.min(y_prediction)])
    highLimit = np.max([np.max(y_test), np.max(y_prediction)])

    ax.plot([lowLimit, highLimit], [lowLimit, highLimit], 'k--', linewidth=1.0)
    scatter = ax.scatter(y_test[idex], y_prediction[idex], c=z[idex], cmap='Spectral_r',alpha=0.9, edgecolor='none', s=15)
    
    ax.set_xlim(np.min(y_test), np.max(y_test))
    ax.set_ylim(np.min(y_prediction), np.max(y_prediction))

    font3 = {'family':'sans-serif','size':16,'color':'k'}

    cbar = plt.colorbar(scatter,shrink=1,orientation='vertical',extend='both',pad=0.015,aspect=30)
    cbar.set_ticks([])
    cbar.update_ticks()
    cbar.set_label('Density',fontdict=font3)

    plt.ylabel("DFT Force (eV/A)",fontdict=font3)
    plt.xlabel("NEP Force (eV/A)",fontdict=font3)
    ax.axis('equal')
    ax.axis('square')

    rmse = np.sqrt(np.mean((y_test-y_prediction)**2))
    plt.text(np.mean(y_test), 
             np.min(y_prediction),
             "RMSE: {:.3f} eV/A".format(rmse), fontsize=12)


def pltEng(eT, eP, npltdata=100000):

    eT = eT - np.mean(eT)
    eP = eP - np.mean(eP)

    if len(eT) > npltdata:
        tmpidex = random.sample(range(len(eT)), npltdata)
        y_test = eT[tmpidex]
        y_prediction = eP[tmpidex]
    else:
        y_test = eT
        y_prediction = eP

    # calculate gaussian_kde
    from scipy.stats import gaussian_kde
    #import matplotlib.cm as cm
    xy = np.vstack([y_test, y_prediction])
    z = gaussian_kde(xy)(xy)
    idex = np.lexsort([z])
    fig,ax=plt.subplots()

    # plt.title("NEP energys vs DFT energy", fontsize=16)
    ax.set_aspect(1)
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    
    ymajorFormatter = ticker.FormatStrFormatter('%.2f') 
    xmajorFormatter = ticker.FormatStrFormatter('%.2f') 
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    
    ax.spines['bottom'].set_linewidth(2)
    ax.spines['left'].set_linewidth(2)
    ax.spines['right'].set_linewidth(2)
    ax.spines['top'].set_linewidth(2)

    ax.tick_params(labelsize=14)

    lowLimit = np.min([np.min(y_test), np.min(y_prediction)])
    highLimit = np.max([np.max(y_test), np.max(y_prediction)])

    ax.plot([lowLimit, highLimit], [lowLimit, highLimit], 'k--', linewidth=1.0)
    scatter = ax.scatter(y_test[idex], y_prediction[idex], c=z[idex], cmap='Spectral_r', alpha=0.9, edgecolor='none', s=30)
    
    ax.set_xlim(np.min(y_test), np.max(y_test))
    ax.set_ylim(np.min(y_prediction), np.max(y_prediction))

    font3 = {'family':'sans-serif','size':16,'color':'k'}
    cbar = plt.colorbar(scatter,shrink=1,orientation='vertical',extend='both',pad=0.015,aspect=30)
    cbar.set_ticks([])
    cbar.update_ticks()
    cbar.set_label('Density',fontdict=font3)
    
    plt.ylabel("DFT energy (eV/atom)",fontdict=font3)
    plt.xlabel("NEP energy (eV/atom)",fontdict=font3)
    ax.axis('equal')
    ax.axis('square')

    rmse = np.sqrt(np.mean((y_test-y_prediction)**2))
    plt.text(np.mean(y_test), 
             np.min(y_prediction),
             "RMSE: {:.3f} eV/atom".format(rmse), fontsize=12)
    
    # figname = 'eng.TvsP.square.NEP'+'.png'
    # fig.savefig(figname, format='png', bbox_inches='tight', transparent=True, dpi=600)

pltEng(e_nep, e_dft)
pltForce(f_nep, f_dft)