'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-04-26 02:52:52
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-04-27 18:48:13
'''

import os
import matplotlib
import numpy as np
from ase.io import read, write
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from adjustText import adjust_text


'''
apps: n2p2 or nep                                                type: str
data: images data or training or testing results from software.  type: class<Atoms> or file np.loadtxt can read.
(depends on resource parameters)
resource: software or images                                     type: str
'''
def data_normalization(apps, data, resource):
# NOTE：Now apps only support n2p2 and nep.
    if resource == 'software':
        e_f_dat = np.loadtxt(data)
        
        
        if apps == 'nep':
            if e_f_dat.shape[1] == 2:
                if e_f_dat.shape[0] == len(imgs):
                    img_num_matrix = np.arrange(0, len(imgs)).reshape(-1, 1)
                    normalized_array = np.concatenate((img_num_matrix, e_f_dat[:, [1, 0]]), axis=1)
                    np.savetxt('energy.data', normalized_array, fmt='%s\t%s\t%s')
                else:
                    print('Error! The number of data and images is different! ')
                    
            elif e_f_dat.shape[1] == 6:
                atom_num = sum(map(lambda x : len(x), imgs))
                if e_f_dat.shape[0] == atom_num:
                    img_atom_matrix = []
                    for i in range(len(imgs)):
                        for j in range(len(imgs[i])):
                            img_atom_matrix.append([i, j])
                    normalized_array = np.concatenate((img_atom_matrix, e_f_dat[:, [3, 4, 5, 0, 1, 2]]), axis=1)
                    np.savetxt('force.data', normalized_array, fmt='%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s')
                    
                else:
                    print('Error! The number of data and images is different! ')
                    

        elif apps == 'n2p2':
            if e_f_dat.shape[1] == 3:
                np.savetxt('energy.data', e_f_dat, fmt='%s\t%s\t%s')
            elif e_f_dat.shape[1] == 4:
                atom_num = int(e_f_dat.shape[0] / 3)
                # normalized_array = [[e_f_dat[i + j][k] for k in range(4)] for j in range(3) for i in range(atom_num)]
                #NOTE: Need to check the speed between for loop and list comprehension
                normalized_array = []
                for i in range(atom_num):
                    line = [e_f_dat[i+2][0], e_f_dat[i+2][1], e_f_dat[i][2], e_f_dat[i+1][2], e_f_dat[i+2][2], e_f_dat[i][3], e_f_dat[i+1][3], e_f_dat[i+2][3]]
                    normalized_array.append(line)
                
                np.savetxt('force.data', normalized_array, fmt='%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s')
        else:
            print(f'The {apps} software is not supported at the moment')
                
# TODO: When the input are structures. How to generate the normalized data.                  
    elif resource == 'images':
        try:
            imgs = read(data, ':')
        except Exception as e:
            print("An error occured:", e)
        pass

def rmse_calculater(T, P):
    T, P = np.array(T), np.array(P)
    error = T - P
    RMSE = np.sqrt(np.sum((error) ** 2) / len(T))
    return error, RMSE
        
def err_structure_finding(error_bar, images, otype, replace_atom='Au', Cutimg=False):
    if os.path.exists('energy.data'):
        data = np.loadtxt('energy.data')
        error, RMSE = rmse_calculater(data[:, 1], data[:, 2])
        print('Energy RMSE of data : ' + str(RMSE))
        err_indices = np.where(np.abs(error) > error_bar * RMSE)[0]
        np.savetxt('Err-str.txt', data[err_indices], fmt='%s\t%s\t%s')
        err_img = []
        for index in err_indices:
            err_img.append(images[index])
        write(f'Err-energy.{otype}', err_img, format=otype)
        
        if Cutimg:
            leave_img_id = [x for x in range(len(images)) if x not in err_indices]
            leave_img = [images[index] for index in leave_img_id] 
            write(f'cutE-img.{otype}', leave_img, format=otype)
            
    elif os.path.exists('force.data'):
        data = np.loadtxt('force.data')
        error, RMSE = rmse_calculater(data[:, 2:5], data[:, 5:8])
        print('Force RMSE of data : ' + str(RMSE))
        frr_indices = np.argwhere(np.abs(error) > error_bar * RMSE)
        frr_output = [[data[xx[0], 0], data[xx[0], 1], data[xx[0], xx[1]+2], data[xx[0], xx[1]+5]] for xx in frr_indices ]
        # err_e_f_dat = data[frr_indices]
        np.savetxt('Err-str.txt', frr_output, fmt='%s\t%s\t%s\t%s')
        bank = np.array(frr_output)[:, 0:2]
        bank1 = np.unique(bank, axis=0).astype(float).astype(int)
        dic = {}
        for i in bank1:
            if i[0] in dic:
                dic[i[0]].append(i[1])
            else:
                dic[i[0]] = [i[1]]
        print('The error img dictionary is as follows:')
        print(dic)
        img_info = sorted(dic.items())
        if os.path.exists(f'Frr-force.{otype}'):
            os.remove(f'Frr-force.{otype}')
        for k in img_info:
            for j in k[1]:
                if replace_atom == 'Au':
                    print(f'Attention! You don\'t input the replace_atom parameter, the error atom will be replaced as {replace_atom}.')
                    images[k[0]].symbols[j] = replace_atom
                elif type(replace_atom) == dict:
                    images[k[0]].symbols[j] = replace_atom[images[k[0]].symbols[j]]
                else:
                    print('Error! Invalid input of replace_atom!')
            images[k[0]].write(f'Frr-force.{otype}', format=otype, append=True)
        print('The error structure have writen down.')
        err_img = [yy[0] for yy in img_info]
        if Cutimg:
            leave_img_id = [x for x in range(len(images)) if x not in err_img]
            leave_img = [images[index] for index in leave_img_id] 
            write(f'cutF-img.{otype}', leave_img, format=otype)
    else:
        print('No mormalized data file!')

    
def plot_scatter(x, y, Err_commennt=True, colors=['blue','green','orange'], id_adjust=False):
    matplotlib.use('Agg')
    font3 = {'family':'Times','size':16,'color':'k'}
    matplotlib.rcParams['xtick.direction'] = 'in'
    matplotlib.rcParams['ytick.direction'] = 'in' # in, out or inout
    matplotlib.rcParams['ytick.right'] = True
    matplotlib.rcParams['xtick.top'] = True
    fontsize = 16
    plt.rcParams['font.size'] = fontsize
    plt.rcParams['font.family'] = 'Times'
    plt.rcParams['font.weight'] = "heavy"
    plt.figure(figsize=(12, 8))
    # plt.subplot(121)
    
    error, RMSE = rmse_calculater(x, y)
    lowLimit = np.min([np.min(x), np.min(y)])
    highLimit = np.max([np.max(y), np.max(y)])
    if x.shape[1] == y.shape[1] == 1:
        plt.scatter(x, y, c=colors[2], s=1, alpha=0.5)
        plt.ylabel("DFT energy (eV/atom)", fontdict=font3)
        plt.xlabel("NEP energy (eV/atom)", fontdict=font3)
        plt.text(highLimit,lowLimit,
        "Energy RMSE: {:.3f} eV/atom".format(RMSE),
        ha='right', va='bottom',fontsize=12)
    elif x.shape[1] == y.shape[1] == 3:
        color_map = [colors[i % 3] for i in range(x.shape[1]) for _ in range(x.shape[0])]
        plt.scatter(x, y, c=color_map, s=1, alpha=0.3)
        plt.ylabel("DFT energy (eV/Å)", fontdict=font3)
        plt.xlabel("NEP energy (eV/Å)", fontdict=font3)
        plt.text(highLimit,lowLimit,
        "Force RMSE: {:.3f} eV/Å".format(RMSE),
        ha='right', va='bottom',fontsize=12)
    else:
        print('X Y label setting is default.')
    plt.plot([lowLimit,highLimit],[lowLimit,highLimit],'--',c='black',linewidth=1)

    if Err_commennt:
        err_data = None
        if os.path.exists('Err-str.txt'):
            err_data = np.loadtxt('Err-str.txt')
        else:
            print('WARNING! You need a Err-str.txt file generate by Err_structure_finding function. The comment will not be added.')
        if err_data.shape[1] == 3:
            print('The loading file is energy error data.')
            err_img = err_data[:, 0]
            err_point = err_data[:, 1:3]
            if id_adjust:
                text = [plt.annotate(f'{err_img[i]}', err_point[i], textcoords="offset points", xytext=(1,1), ha='center',color='black',fontsize=5) for i in range(len(err_data))]
                adjust_text(text, arrowprops=dict(arrowstyle='->', lw=0.4, color='blue'))
            else:
                for i in range(len(err_data)):
                    plt.annotate(f'{err_img[i]}', err_point[i], textcoords="offset points", xytext=(1,1), ha='center',color='black',fontsize=5)
        elif err_data.shape[1] == 4:
            print('The loading file is force error data.')
            err_img = err_data[:, 0]
            err_img_atom = err_data[:, 1]
            err_point = err_data[:, 2:4]
            if id_adjust:
               text = [plt.annotate(f'{err_img[i]}-{err_img_atom[i]}', err_point[i], textcoords="offset points", xytext=(0,0), ha='center',color='black',fontsize=5) for i in range(len(err_data))]
               adjust_text(text, arrowprops=dict(arrowstyle='->', lw=0.4, color='blue'))
            else:
                for i in range(len(err_data)):
                    plt.annotate(f'{err_img[i]}-{err_img_atom[i]}', err_point[i], textcoords="offset points", xytext=(0,0), ha='center',color='black',fontsize=5) 
        else:
            print('Invalid Err_str.txt file!')
        plt.savefig('Error.png', dpi=300)
    
        # plt.subplot(122)
        
        
def main(trajname, apps, data=None, er_bar=1.5, out_type='traj', ra='Au', cut_img=True, text_adjust=False):
    a = trajname
    if data is None:
        data_normalization(apps, trajname, 'images')
    else:
        data_normalization(apps, data, 'software')
        
    traj = read(a, ':')
    err_structure_finding(error_bar=er_bar, images=traj, otype=out_type, replace_atom=ra, Cutimg=cut_img)
    if os.path.exists('force.data'):
        newdata = np.loadtxt('force.data')
        plot_scatter(x=newdata[:, 2:5], y=newdata[:, 5:8], Err_commennt=True, colors=['#ef5674', '#eebc59','#5891d5'], id_adjust=text_adjust)
    elif os.path.exists('energy.data'):
        newdata = np.loadtxt('energy.data')
        plot_scatter(x=newdata[:, 1].reshape(-1, 1), y=newdata[:, 2].reshape(-1, 1), Err_commennt=True, colors=['#ef5674', '#eebc59','#5891d5'], id_adjust=text_adjust)
        
if __name__ == '__main__':
    main(trajname='train.traj', apps='n2p2', data='trainpoints.000020.out', er_bar=15, ra={'Ru':'Au', 'Si':'Ge'}, text_adjust=False)