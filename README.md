<!--
 * @Author: glab-cabage 2227541807@qq.com
 * @Date: 2024-04-22 21:14:09
 * @LastEditors: glab-cabage 2227541807@qq.com
 * @LastEditTime: 2024-06-03 10:44:56
 * @FilePath: /tools-for-mlp-in-atomic-simulation/README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# Tools for MLP in atomic simulation


## Brief discription
This tool is used to accomplish file convert.
We have totally four usages, two of them are from one file to one file and many of your target type files, and when you select the second of these two usages, you can select whether distribute these files into different folders.
## Usage
The other two usages are from many same type files to one file or many of your target type files.
The four scripts as follows:
1. one-to-one.py
2. one-to-mlt.py
3. mlt-to-one.py
4. mlt-to-mlt.py

1、 python one-to-one.py  filename      output-name         output-type        constraint-or-not       output-is-traj-or-not  input-lmpdata-or-not


Attention: 
(1) If lmpdat=True, you need a replacement file named "elements.txt".
(2) If you want fix some atoms, you need set the fixed ID into a file name fixatom.txt.
```
python one-to-one.py POSCAR new xsd 0 0 0
python one-to-one.py train.traj train extxyz 0 1 0
python one-to-one.py new.xsd new vasp 1 0 0
python one-to-one.py nvt.dump surface extxyz 0 1 1
```


2、 python one-to-mlt.py lammps-data-or-not  filename  output-path  output-type  whether-need-sparate-each-file-in-a-folder


Attention:
(1) If the first parameter "lmpdata" is True, you need a replacement file named "elements.txt" , or the elements will be set as lammps default type.

(2) If you want to take special frames from the whole trajectory, you only need a file named "frames.txt" or "frames-range.txt", the   output will be each ID image in "frames.txt" and each range images from each line. If you don't have the file, the output folder will cover whole trajectory.

(3) If the first option is 1, it means the input file type is lammps-data, you can use a elements.txt file, input what elements you want change, it will change the elements in the output file.


```
python one-to-mlt.py 1 train.xyz dft-cal vasp 1
```

3、 python mlt-to-one.py  filename  output-name  output-type  whether-files'-name-same  lmpdata-or-not
Attention: 
(1) if filenames are all same, the first input is filename, or it will become the suffix of filenames
(2) If lmpdat=True, you need a replacement file named "elements.txt".
```
python mlt-to-one.py  OUTCAR  train traj 1 0
python mlt-to-one.py  xsd new extxyz 0 0
python mlt-to-one.py  nvt.dump train extxyz 1 1
```

4、 python mlt-to-mlt.py  filename  output-path  output-type   constraint-or-not    whether-files'-name-same
Attention: if you want fix some atoms, you need set the fixed ID into a file name fixatom.txt.
```
python mlt-to-mlt.py CONTCAR for-lammps-cal lammps-data 1 0
python mlt-to-mlt.py cif for-dft vasp 0  1#(Your path need a file name fixatoms.txt)
```

