#! /usr/bin/env bash
printf "%-16s%-32s%-16s\n" " " "CPU Occupation Detail" " "
printf "%-16s%-16s%-16s%-16s\n" "GROUP" "MAX_CORE" "RUN_CORE" "IDLE_CORE"
for i in short medium large debug ser;
do
 sum_core=$( bhosts  hg_$i |grep -v HOST_NAME |awk '{sum +=$4 } END {print sum}')
 run_core=$(bhosts  hg_$i |grep -v HOST_NAME |awk '{sum +=$5 } END {print sum}')
 leave_core=$(echo "$sum_core - $run_core " |bc )
 printf "%-16s%-16s%-16s%-16s\n" "$i" "$sum_core" "$run_core" "$leave_core"
done |sort -V
node=("gpu" "smp")
se=("gpu" "s00")
for j in  0 1
do
 sum_core=$(bhosts |grep ${se[j]} | awk '{sum +=$4} END {print sum}')
 run_core=$(bhosts |grep ${se[j]} | awk '{sum +=$5} END {print sum}')
 leave_core=$(echo "$sum_core - $run_core " |bc )
 printf "%-16s%-16s%-16s%-16s\n" "${node[j]}" "$sum_core" "$run_core" "$leave_core"
 done |sort -V
