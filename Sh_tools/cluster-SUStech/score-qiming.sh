#! /usr/bin/env bash

printf "%-16s%-32s%-16s\n" " " "CPU Occupation Detail" " "
printf "%-16s%-16s%-16s%-16s\n" "GROUP" "MAX_CORE" "RUN_CORE" "IDLE_CORE"
for i in `bqueues  |awk '{print $1}' |grep -v QUEUE_NAME |grep -v dataq |grep -v test`;
do
 sum_core=$( bhosts  hg_$i |grep -v HOST_NAME |awk '{sum +=$4 } END {print sum}')
 run_core=$(bhosts  hg_$i |grep -v HOST_NAME |awk '{sum +=$5 } END {print sum}')
 leave_core=$(echo "$sum_core - $run_core " |bc )
 printf "%-16s%-16s%-16s%-16s\n" "$i" "$sum_core" "$run_core" "$leave_core"
done |sort -V
echo -e "\n"
printf "%-16s%-32s%-16s\n" " " "CPU Occupation Detail" " "
printf "%-16s%-16s%-16s%-16s\n" "GROUP" "MAX_GPU" "RUN_GPU" "IDLE_GPU"
for i in `bqueues  |awk '{print $1}' |grep -v QUEUE_NAME |grep -v dataq |grep -v test`;
do
 sum_core=$( bhosts -gpu hg_$i |grep -v HOST_NAME |awk '{sum +=$6 } END {print sum}' )
 run_core=$(bhosts -gpu hg_$i |grep -v HOST_NAME |awk '{sum +=$7 } END {print sum}')
 leave_core=$(echo "$sum_core - $run_core " |bc )
 printf "%-16s%-16s%-16s%-16s\n" "$i" "$sum_core" "$run_core" "$leave_core"
done |grep -v "bhosts:" | sort -V
