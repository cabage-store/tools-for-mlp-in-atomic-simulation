#!/bin/bash

change_directory() {
        state=$(bjobs |grep $1 | awk '{print $3}')
       	if [ "$state" = "RUN" ];then
         path=$(bjobs -l $1 | tr -d '\n ' | grep -oP '(?<=ExecutionCWD<).*?(?=>)')
	 echo "$path"
	 cd $path
	elif [ "$state" = "PEND" ];then
	 path=$(bjobs -l $1 | tr -d '\n ' | grep -oP '(?<=CWD<).*?(?=>)')
         echo "$path"
	 cd $path
        fi
	    } 
change_directory $1
