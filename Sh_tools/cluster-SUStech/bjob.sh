#!/usr/bin/env bash

arr=($(bjobs | awk '{print $1}'))
if [ "${arr[0]}" != "JOBID" ];then
  echo 'There is no job submitted!'
else
 j=1
 for ((i=0; i<${#arr[@]}; i++))
 do
  line_content=$(bjobs | awk -v line=$(($i+1)) 'NR==line')
  list_num=$(echo "$line_content" |awk '{print NF}')
 if [ "${arr[i]}" = "JOBID" ]; then
  printf "%-5s%-85s%-23s%-20s\n" "No" "${line_content}" "START_TIME" "PATH"
 elif [ "${arr[i]}" != "JOBID" ] && [ "${list_num}" -gt 1 ];then
  state=$(echo "$line_content" |awk '{print $3}')
  if [ "$state" = "RUN" ];then
   start_time=$(bjobs -l ${arr[i]} | grep -oP ".*?(?=: Started).*?")
   path=$(bjobs -l ${arr[i]} | tr -d '\n ' | grep -oP '(?<=ExecutionCWD<).*?(?=>)')
   printf "%-5s%-85s%-23s%-20s\n" "$j" "${line_content}" "${start_time}" "${path}"
   j=$(($j+1))
  elif [ "$state" = "PEND" ];then
   start_time="--------"
   path=$(bjobs -l ${arr[i]} | tr -d '\n ' | grep -oP '(?<=CWD<).*?(?=>)')
   printf "%-5s%-85s%-23s%-20s\n" "$j" "${line_content}" "${start_time}" "${path}"
   j=$(($j+1))
  fi
 else
  printf "%-5s%-10s\n" " "  "$line_content"
 fi
 done
fi
