#! usr/bin/env bash
current_date=$(date+"%Y-%m-%d")
for ijob in `find -name "in.*" -exec dirname {} \; |sort -V`
do 
 cd $ijob
 if [ ! -f log.lammps ];then
  while true
  do
   if  [ $(bjobs | grep "PEND" |wc -l) -lt $1 ];then
    bsub<$2
    echo "[completed]: `pwd`" >> /work/$USER/$current_date-lmpqsub.log
    echo `date` >> /work/$USER/$current_date-lmpqsub.log
    break
   else
    echo "PEND job larger than $1, please wait 1 minute."
    sleep 1m
   fi
  done
 fi
 cd $OLDPWD
done
