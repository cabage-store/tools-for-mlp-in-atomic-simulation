#! usr/bin/env bash
for ijob in `find -name POSCAR -exec dirname {} \; |sort -V`
do 
 cd $ijob
 if [ ! -f OUTCAR ];then
  while true
  do
   if  [ $(bjobs | grep "PEND" |wc -l) -lt $1 ];then
    bsub<$2
    end_time=`date`
    echo "[completed]: `pwd`" >> /work/$USER/$current_date-lmpqsub.log
    echo `date` >> /work/$USER/$current_date-lmpqsub.log
    break
   else
    echo "PEND job larger than $1, please wait 1 minute."
    sleep 1m
   fi
  done
 fi
 cd $OLDPWD
done
