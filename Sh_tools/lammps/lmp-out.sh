#! /usr/bin/env bash 
option=$1
start_line=$(echo "$(grep -n "Step          Time" log.lammps | sed -n "$1p" |cut -d ":" -f 1) + 1" |bc )
end_line=$(echo "$(grep -n "Loop time" log.lammps | sed -n "$1p" |cut -d ":" -f 1) - 1" |bc )
if [ $end_line -eq -1 ];then
 end_line=$(echo "$(wc -l log.lammps |awk '{print $1}')")
fi
echo -e "The data in log.lammps starts from \e[91m$start_line\e[0m to \e[91m$end_line\e[0m."
outs=()
index=0
for v in "${@:2}";do
 out=$(grep  " $v " log.lammps | sed -n "$1p" | awk -v num="$v" '{for (i=1; i<=NF; i++) if($i == num) print i}')
 echo -e "The result for \e[94m$v\e[0m is the \e[92m$out\e[0m clolumn."
 index=$((index + 1))
 outs+=("temp$index.out")
 awk -v start=$start_line -v end=$end_line 'NR>=start && NR<=end' log.lammps | grep -v "colvars" |grep -v "stderr"| awk -v column=$out '{print $column}' >temp$index.out
done
paste -d "\t" "${outs[@]}" | column -t -s $'\t' > lmp-diy.out
/bin/rm "${outs[@]}"

