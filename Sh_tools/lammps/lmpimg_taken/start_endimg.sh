startline=$(grep -n TIMESTEP $1 | head -n $2 |tail -n 1 | cut -d: -f1)
endline=$(echo "$(grep -n TIMESTEP $1 | head -n $(echo "$3+1" | bc) |tail -n 1 | cut -d: -f1) - 1" | bc)
sed  -n  "$startline, $endline"p $1 > $2_$3.dump
