#! /usr/bin/env bash
start_line=$(grep -n "ITEM: TIMESTEP" $1 |tail -n 1 |awk -F':' '{print $1}')
awk -v line=$start_line 'NR>=line' $1 > last-img.dump
