#! usr/bin/env bash
if [ -f frames.dump ];then
rm frames.dump
fi
for i in `cat frames.txt`
do 
 startline=$(grep -n TIMESTEP $1 | head -n $i |tail -n 1 | cut -d: -f1)
 endline=$(echo "$startline+$2+8" | bc)
 sed -n "$startline, $endline"p $1 >> frames.dump
done
