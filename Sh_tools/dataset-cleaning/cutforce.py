#!usr/bin/env python
#-*- utf-8 coding -*-
'''
date: 2023-11-13
author: Jiaqi Wang
usage: To cut the structure of Force >10

'''
import argparse
from ase.io import read, write, Trajectory
from pynep.io import load_nep,dump_nep
import numpy as np

parser = argparse.ArgumentParser(description='cut force beyond the setting threshold.')

parser.add_argument('traj', help='input file path')
parser.add_argument('-t', '--type', help='use tools to read file nep or ase', default='ase')
parser.add_argument('-f', '--force', help='force tolerance', default=10, type=float)
parser.add_argument('-o', '--output', help='output file path', default='filtered.traj')

args = parser.parse_args()

traj = args.traj
ftype = args.type.lower()
if traj.endswith('.xyz'):
    if ftype == 'nep':
        imgs = load_nep(traj,ftype='exyz')
    elif ftype == 'ase':
        imgs = read(traj, index=':')
elif traj.endswith(".traj"):
    imgs = Trajectory(traj, 'r')

newtraj = []
for i in range(len(imgs)):
    force = imgs[i].get_forces()
    max_force = np.max(force)
    min_force = np.min(force)
    if max_force > args.force:
        print(f'image {i} max force is {max_force}, beyond the tolerance {args.force}')
    elif min_force < -args.force:
        print(f'image {i} min force is {min_force}，beyond the tolerance {-args.force}')
    else:
        newtraj.append(imgs[i])
write(args.output, images=newtraj)
print(f'Initial total images number is {len(imgs)} and {len(imgs)-len(newtraj)} images have been cut')
