# -*- coding: utf-8 -*-
from ase.io import read, write
import argparse
import random
usage = argparse.ArgumentParser(description='make two file combined')
usage.add_argument('files', nargs='+', help='input file path ase can read')
usage.add_argument('-o', '--output', help='output file name', default='f_train.xyz')
args = usage.parse_args()
imgs = []
for file in args.files:
    imgs.extend(read(file, ':'))
#random.shuffle(imgs)
write(args.output, images=imgs)
print(f'{len(args.files)} have combined in output file:{args.output}')
