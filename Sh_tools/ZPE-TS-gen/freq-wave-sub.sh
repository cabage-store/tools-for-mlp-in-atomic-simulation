#! /usr/bin/env bash 
mkdir freq sol
cp CONTCAR POTCAR INCAR KPOINTS mytest.lsf ./freq
cd freq
echo -e "402\n2\n1\n$1\nall" |vaspkit
mv CONTCAR_FIX POSCAR
rm CONTCAR SELECTED_ATOMS_LIST
cp ~/bin/job_lsf/job-rusi/freq/INCAR .
sed -i "/BSUB -J/s/sol/freq/g" mytest.lsf
#bsub<mytest.lsf
cd $OLDPWD
cd sol;mkdir get-wave sol
cd ..
cp CONTCAR POTCAR INCAR KPOINTS mytest.lsf ./sol/get-wave/
cd ./sol/get-wave/
mv CONTCAR POSCAR
cp ~/bin/job_lsf/job-rusi/get-wave/INCAR .
sed -i "/BSUB -J/s/sol/get-wave/g" mytest.lsf
#bsub<mytest.lsf
cd ../../
