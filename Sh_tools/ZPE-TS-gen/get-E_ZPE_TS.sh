#! /usr/bin/env bash
E=$(grep without ./sol/sol/OUTCAR |tail -n 1 |awk '{print $7}')
cd freq
ZPE=$(echo -e "501\n298.15" |vaspkit |grep E_ZPE |awk '{print $7}')
TS=$(echo -e "501\n298.15" |vaspkit |grep "Entropy contribution" |awk '{print $7}')
echo -e "$(basename $(dirname $(pwd)))\t$E\t$ZPE\t$TS"
cd $OLDPWD
