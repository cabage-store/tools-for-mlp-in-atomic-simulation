#!/usr/bin/env python
#Get Lattice paramters from BM state equation
# Written by Qiang, edited by Jiaqi 
# To use it : python bm.py

import math, sys
import numpy as np

v = sys.argv
print( """
Warning!!!!

Remember to change a*2.8664 in x 
to your own lattice parameters before use it!

""")
#get lattice parameters and corresponding energies from data file 
# a is the list of sclaing factors in the test calculations 
# and E is the list of related energies 

a, E  = np.loadtxt(v[1], usecols=(0,1), delimiter='\t', unpack = True)	

# Change item below a*2.8664 into your parameters 

x  = (a*float(v[2]))**(-2)

# if column 1 in data file is not a list of sclaing factors, 
# for example, it is a list of parameters, use x = a**(-2) 

# equation fitting: https://docs.scipy.org/doc/numpy/reference/generated/numpy.polyfit.html

p = np.polyfit(x, E, 3)

c0 = p[3]
c1 = p[2]
c2 = p[1]
c3 = p[0]

#print 'The fitted BM equation is:'
#print ' y = %.4f * (x**3) + %.4f * (x**2) + %.4f * (x) + %.4f' %(c3,c2,c1,c0)

# Get the results of c_1 + 2c_2x + 3c_3x^2 = 0 
x1 = (math.sqrt(4*c2**2 - 12*c1*c3) - 2*c2)/(6*c3) 
para = 1/math.sqrt(x1)

print ('The final lattice parameter is: %s  ' %(para)) 
