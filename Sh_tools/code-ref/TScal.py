#!/usr/bin/env python
# -*- coding: utf-8  -*-
import sys
import math
from scipy import constants as con

script,nu = sys.argv
nu = float(nu)*100  # Convert cm-1 to m-1

h_p = con.h    # 6.62606957e-34  # J*s Plank Constant
k_b = con.k    # 1.38064852e-23  # m2 * kg*s-2*k-1 Boltzman Constant
R_gas = con.R  # 8.3144598       # J*mol-1*k-1 Gas Constant
l_s = con.c    # 299792458       #light speed m* s-1
Tem = 300      # Temperature  300 K
beta = 1/(k_b *Tem)

def get_pf(nu): #get partition fuction
    x_i = h_p *float(nu) * l_s * beta
    pf_l = x_i / (math.exp(x_i) -1)  #left part in the entropy equation
    pf_r = math.log(1 - math.exp(-x_i))
    pf   = pf_l -pf_r
    entropy = R_gas * pf
    return entropy
entropy = get_pf(nu)  #J * K-1 * mol -1
ts      = entropy * Tem / 1000 / 98.485  #in eV
print('%.4f\t %.4f' %(entropy,ts))