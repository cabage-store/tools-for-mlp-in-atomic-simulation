# -*- coding: utf-8 -*-
"""
Created on Sat Nov 25 22:11:17 2023

@author: kel91
"""
from ase.io import write, read
from ase.build import molecule
from ase.calculators.emt import EMT
from ase.neb import NEB
from ase.optimize.fire import FIRE as QuasiNewton
import sys
import ase
import os

v = sys.argv
initial = read(v[1])
final = read(v[2])

# Generate blank images.
images = [initial]

for i in range(int(v[3])):
    images.append(initial.copy())

for image in images:
    image.calc = EMT()

images.append(final)

# Run linear interpolation.
neb = NEB(images)
neb.interpolate('idpp')

for i in range(len(images)):
    name = '0' + str(i)
    os.mkdir(name)
    write(name + '/POSCAR', images[i])
