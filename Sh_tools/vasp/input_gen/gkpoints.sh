#! /mse-wangjq/bin/bash

# To generate KPOINTS file
# Written by Jiaqi Wang on 2022-08-03
# To use it: kpoints.sh
if [ -f KPOINTS ];then
 /bin/rm KPOINTS
fi
echo "KPOINTS file Generate by Jq-Pyshtools" >KPOINTS    # First Line
echo 0 >> KPOINTS                
if [ "$1" = "mp" ];then
 echo "Monkhorst-Pack" >> KPOINTS
 echo $2 $3 $4 >>KPOINTS
 echo 0 0 0 >> KPOINTS
elif [ "$1" = "gamma" ];then
 echo "Gamma-Centered" >> KPOINTS
 echo $2 $3 $4 >>KPOINTS
 echo 0 0 0 >> KPOINTS
elif [ "$1" = "auto" ];then
 echo "Auto" >> KPOINTS
 echo $2 >> KPOINTS
else
 echo "[INFO]: Error! This kinds of KPOINTS not implemented!"
 rm KPOINTS
fi
