#! usr/bin/env bash
if [ -f E-result.log ];then
/bin/rm E-result.log
fi
echo -e "File PATH\tEnergy\tIonic Step\tFirst Electron Step" >> temp_energy.out
for i in `find . -name OUTCAR -exec dirname {} \;|sort -V`
do
ion_step=$(grep "F" $i/OSZICAR |wc -l)
ele1_step=$(grep -B 1  " 1 F" $i/OSZICAR |grep -v F |awk '{print $2}')
converge=$(grep "reached required" $i/OUTCAR |wc -l)
energy=$(grep "without" $i/OUTCAR |tail -n 1 |awk '{print $7}')
echo -e "$i\t${energy}\t${ion_step}\t${ele1_step}" >> temp_energy.out
done
column -t -s $'\t' temp_energy.out > E-result.log
/bin/rm temp_energy.out
max=$(awk 'NR==2 {max=$2} $2>max {max=$2} END {print max}' E-result.log)
min=$(awk 'NR==2 {min=$2} $2<min {min=$2} END {print min}' E-result.log)
echo -e "Max value in path $(awk -v max="$max" '$2==max {print $1 " is: " $2}' E-result.log)"
echo -e "Min value in path $(awk -v min="$min" '$2==min {print $1 " is: " $2}' E-result.log)"
