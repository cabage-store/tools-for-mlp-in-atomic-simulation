#! /usr/bin/env bash
/bin/rm unconfile.txt
mkdir un-converged
j=0
for i in `find ./ -path ./un-converged -prune -o -name OSZICAR -exec dirname {} \;|grep -v un-converged |sort -V`;
do
a=$(grep "F=" $i/OSZICAR)
b=$(awk '/F=/{print prev}{prev=$2}' $i/OSZICAR)
if [[ -z $a || $b -gt $1 ]];then
echo "Vasp job in path $i is not converge, move this folder into the un-converged folder."
echo "$i">>unconfile.txt
mv $i ./un-converged/$j
j=$(($j+1))
fi
done
