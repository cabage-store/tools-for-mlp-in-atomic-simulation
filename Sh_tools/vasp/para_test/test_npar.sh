#! usr/bin/env bash
mkdir NPAR
for j in $@
do mkdir $j
cp POSCAR POTCAR INCAR KPOINTS mytest.lsf ./$j/
npar_value=$(grep  "Square of NPAR" INCAR|awk '{print $3}')
sed -i "/Square of NPAR/s/$npar_value/$j/g" ./$j/INCAR
mv $j ./NPAR
done
