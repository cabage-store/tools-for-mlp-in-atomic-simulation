#! usr/bin/env bash
mkdir ENCUT
for i in $@
do mkdir $i
cp POSCAR POTCAR INCAR KPOINTS mytest.lsf ./$i/
encut_value=$(grep "ENCUT" INCAR |awk '{print $3}')
sed -i  /ENCUT/s/$encut_value/$i/g ./$i/INCAR
mv $i ENCUT
done
