'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-04-26 02:52:52
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-07-24 16:01:31
'''

import os
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from pynep.calculate import NEP
import matplotlib.ticker as ticker
from adjustText import adjust_text
from Plottools.plotfig import font_dict
from ase.io import read, write, Trajectory
from matplotlib.ticker import MultipleLocator

'''
apps: n2p2 or nep                                                type: str
data: images data or training or testing results from software.  type: class<Atoms> or file np.loadtxt can read.
(depends on resource parameters)
resource: software or images                                     type: str
'''
def data_normalization(apps, imgdata, resource, data=None, pot=None):
# NOTE：Now apps only support n2p2 and nep.
    if resource == 'software':
        imgs = imgdata
        if data is not None:
            e_f_dat = np.loadtxt(data)
        else:
            print(f'You need load a initial data file from {apps} trainging result.')
        
        
        if apps == 'nep':
            if e_f_dat.shape[1] == 2:
                if e_f_dat.shape[0] == len(imgs):
                    img_num_matrix = np.arange(0, len(imgs)).reshape(-1, 1)
                    normalized_array = np.concatenate((img_num_matrix, e_f_dat[:, [1, 0]]), axis=1) # [1, 0] DFT NEP
                    np.savetxt('energy.data', normalized_array, fmt='%4s')
                else:
                    print('Error! The number of data and images is different! ')
                    
            elif e_f_dat.shape[1] == 6:
                atom_num = sum(map(lambda x : len(x), imgs))
                if e_f_dat.shape[0] == atom_num:
                    img_atom_matrix = []
                    for i in range(len(imgs)):
                        for j in range(len(imgs[i])):
                            img_atom_matrix.append([i, j])
                    normalized_array = np.concatenate((img_atom_matrix, e_f_dat[:, [3, 4, 5, 0, 1, 2]]), axis=1)
                    np.savetxt('force.data', normalized_array, fmt='%4s')
                    
                else:
                    print('Error! The number of data and images is different! ')
                    

        elif apps == 'n2p2':
            if e_f_dat.shape[1] == 3:
                np.savetxt('energy.data', e_f_dat, fmt='%4s')
            elif e_f_dat.shape[1] == 4:
                atom_num = int(e_f_dat.shape[0] / 3)
                # normalized_array = [[e_f_dat[i + j][k] for k in range(4)] for j in range(3) for i in range(atom_num)]
                #NOTE: Need to check the speed between for loop and list comprehension
                normalized_array = []
                for i in range(atom_num):
                    line = [e_f_dat[i*3+2][0], e_f_dat[i*3+2][1], e_f_dat[i*3][2], e_f_dat[i*3+1][2], e_f_dat[i*3+2][2], e_f_dat[i*3][3], e_f_dat[i*3+1][3], e_f_dat[i*3+2][3]]
                    normalized_array.append(line)
                
                np.savetxt('force.data', normalized_array, fmt='%4s')
        else:
            print(f'The {apps} software is not supported at the moment')
                
# TODO: When the input are structures. How to generate the normalized data.                  
    elif resource == 'images':
        imgs = imgdata
        if apps == 'nep':
            calc = NEP(pot)
        e_dft, e_mlp, f_dft, f_mlp = [], [], [], []
        img_num_matrix = np.arange(0, len(imgs)).reshape(-1, 1)
        img_atom_matrix = []
        for i, img in enumerate(imgs):
            e_dft.append(img.get_potential_energy() / len(img))
            f_dft.append(img.get_forces())
            img.set_calculator(calc)
            e_mlp.append(img.get_potential_energy() / len(img))
            f_mlp.append(img.get_forces())
            for j in range(len(img)):
                img_atom_matrix.append([i, j])
        e_dft = np.array(e_dft).reshape(-1, 1)
        e_mlp = np.array(e_dft).reshape(-1, 1)
        f_dft = np.concatenate(f_dft, axis=0)
        f_mlp = np.concatenate(f_mlp, axis=0)
        img_atom_matrix = np.array(img_atom_matrix)
        # print(np.shape(e_dft), np.shape(e_mlp), np.shape(img_num_matrix), np.shape(f_dft), np.shape(f_mlp), np.shape(img_atom_matrix))
        ene_normalized_array = np.concatenate((img_num_matrix, e_dft, e_mlp), axis=1)
        force_normalized_array = np.concatenate((img_atom_matrix, f_dft, f_mlp), axis=1)
        print(ene_normalized_array.shape, force_normalized_array.shape)
        np.savetxt('energy.data', ene_normalized_array, fmt='%7s')
        np.savetxt('force.data', force_normalized_array, fmt='%7s')
                
                
        

def rmse_calculater(T, P):
    T, P = np.array(T), np.array(P)
    error = T - P
    RMSE = np.sqrt(np.mean(np.square(error)))
    return error, RMSE
        
def err_structure_finding(error_bar, images, otype, option, fontsize, replace_atom='Au', Cutimg=False, comment=False, adjust=False):
    if option == 'energy':
        try:
            data = np.loadtxt('energy.data')
        except Exception as e:
            print("An error occured when load energy.data:", e)
        error, RMSE = rmse_calculater(data[:, 1], data[:, 2])
        print('Energy RMSE of data : ' + str(RMSE))
        err_indices = np.where(np.abs(error) > error_bar * RMSE)[0]
        np.savetxt('Err-str.txt', data[err_indices], fmt='%4s')
        err_img = []
        for index in err_indices:
            err_img.append(images[index])
        write(f'Err-energy.{otype}', err_img, format=otype)
        
        if Cutimg:
            leave_img_id = [x for x in range(len(images)) if x not in err_indices]
            leave_img = [images[index] for index in leave_img_id] 
            write(f'cutE-img.{otype}', leave_img, format=otype)
        plot_scatter(x=data[:, 1].reshape(-1, 1), y=data[:, 2].reshape(-1, 1), fontsize=fontsize, Err_commennt=comment, colors=['#ef5674', '#eebc59','#5891d5'], id_adjust=adjust)
            
    elif option == 'force':
        try:
            data = np.loadtxt('force.data')
        except Exception as e:
            print("An error occured when load force.data:", e)
        error, RMSE = rmse_calculater(data[:, 2:5], data[:, 5:8])
        print('Force RMSE of data : ' + str(RMSE))
        frr_indices = np.argwhere(np.abs(error) > error_bar * RMSE)
        frr_output = [[data[xx[0], 0], data[xx[0], 1], data[xx[0], xx[1]+2], data[xx[0], xx[1]+5]] for xx in frr_indices ]
        # err_e_f_dat = data[frr_indices]
        np.savetxt('Err-str.txt', frr_output, fmt='%4s')
        bank = np.array(frr_output)[:, 0:2]
        bank1 = np.unique(bank, axis=0).astype(float).astype(int)
        dic = {}
        for i in bank1:
            if i[0] in dic:
                dic[i[0]].append(i[1])
            else:
                dic[i[0]] = [i[1]]
        # print('The error img dictionary is as follows:')
        # print(dic)
        img_info = sorted(dic.items())
        if os.path.exists(f'Frr-force-initial.{otype}'):
            os.remove(f'Frr-force-initial.{otype}')        
        if os.path.exists(f'Frr-force-replaced.{otype}'):
            os.remove(f'Frr-force-replaced.{otype}')
        images_copy = images.copy()
        for k in img_info:
            for j in k[1]:
                if replace_atom == 'Au':
                    print(f'Attention! You don\'t input the replace_atom parameter, the error atom will be replaced as {replace_atom}.')
                    images_copy[k[0]].symbols[j] = replace_atom
                elif type(replace_atom) == dict:
                    images_copy[k[0]].symbols[j] = replace_atom[images[k[0]].symbols[j]]
                else:
                    print('Error! Invalid input of replace_atom!')
                    
            images[k[0]].write(f'Frr-force-initial.{otype}', format=otype, append=True)
            images_copy[k[0]].write(f'Frr-force-replaced.{otype}', format=otype, append=True)
        print('The error structure have writen down.')
        err_img = [yy[0] for yy in img_info]
        if Cutimg:
            leave_img_id = [x for x in range(len(images)) if x not in err_img]
            leave_img = [images[index] for index in leave_img_id] 
            write(f'cutF-img.{otype}', leave_img, format=otype)
        plot_scatter(x=data[:, 2:5], y=data[:, 5:8], fontsize=fontsize, Err_commennt=comment, colors=['#ef5674', '#eebc59','#5891d5'], id_adjust=adjust)
    else:
        raise TypeError('Error! The option can only be energy or force !')
    

    
def plot_scatter(x, y, fontsize, Err_commennt=True, colors=['blue','green','orange'], id_adjust=False):
    matplotlib.use('Agg')
    font3 = {'family':'Times','size':fontsize,'color':'k'}
    matplotlib.rcParams['xtick.direction'] = 'in'
    matplotlib.rcParams['ytick.direction'] = 'in' # in, out or inout
    matplotlib.rcParams['ytick.right'] = True
    matplotlib.rcParams['xtick.top'] = True
    plt.rcParams['font.size'] = fontsize
    plt.rcParams['font.family'] = 'Times'
    plt.rcParams['font.weight'] = "heavy"
    fig, ax = plt.subplots()
    # plt.subplot(121)
    font3 = font_dict('sans-serif', fontsize, 'black', 'bold')
    # Setting major tick locators
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    # Set the major tick formatter
    xmajorFormatter = ticker.FormatStrFormatter(f'%.2f') 
    ymajorFormatter = ticker.FormatStrFormatter(f'%.2f')
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    # Set the line width of the axis border
    ax.spines['bottom'].set_linewidth(2)
    ax.spines['left'].set_linewidth(2)
    ax.spines['right'].set_linewidth(2)
    ax.spines['top'].set_linewidth(2)
    # Set the font size of the tick labels
    # Add a title and legend
    ax.tick_params(labelsize=fontsize-2)
    for label in ax.get_xticklabels() + ax.get_yticklabels():
        label.set_fontweight(font3['weight'])
    
    error, RMSE = rmse_calculater(x, y)
    lowLimit = np.min([np.min(x), np.min(y)])
    highLimit = np.max([np.max(x), np.max(y)])
    if x.shape[1] == y.shape[1] == 1:
        plt.scatter(x, y, c=colors[2], alpha=0.7)
        plt.ylabel("DFT energy (eV/atom)", fontdict=font3)
        plt.xlabel("NEP energy (eV/atom)", fontdict=font3)
        plt.text(highLimit,lowLimit,
        "Energy RMSE: {:.3f} eV/atom".format(RMSE),
        ha='right', va='bottom',fontsize=fontsize)
    elif x.shape[1] == y.shape[1] == 3:
        color_map = [colors[i % 3] for i in range(x.shape[1]) for _ in range(x.shape[0])]
        plt.scatter(x, y, c=color_map, alpha=0.7)
        plt.ylabel("DFT force (eV/Å)", fontdict=font3)
        plt.xlabel("NEP force (eV/Å)", fontdict=font3)
        plt.text(highLimit,lowLimit,
        "Force RMSE: {:.3f} eV/Å".format(RMSE),
        ha='right', va='bottom',fontsize=fontsize)
    else:
        print('X Y label setting is default.')
    plt.plot([lowLimit,highLimit],[lowLimit,highLimit],'--',c='black',linewidth=1)

    if Err_commennt:
        err_data = None
        if os.path.exists('Err-str.txt'):
            err_data = np.loadtxt('Err-str.txt')
        else:
            print('WARNING! You need a Err-str.txt file generate by Err_structure_finding function. The comment will not be added.')
        if err_data.shape[1] == 3:
            print('The loading file is energy error data.')
            err_img = err_data[:, 0]
            err_point = err_data[:, 1:3]
            if id_adjust:
                text = [plt.annotate(f'{err_img[i]}', err_point[i], textcoords="offset points", xytext=(1,1), ha='center',color='black',fontsize=5) for i in range(len(err_data))]
                adjust_text(text)
            else:
                for i in range(len(err_data)):
                    plt.annotate(f'{err_img[i]}', err_point[i], textcoords="offset points", xytext=(1,1), ha='center',color='black',fontsize=5)
        elif err_data.shape[1] == 4:
            print('The loading file is force error data.')
            err_img = err_data[:, 0]
            err_img_atom = err_data[:, 1]
            err_point = err_data[:, 2:4]
            if id_adjust:
               text = [plt.annotate(f'{err_img[i]}-{err_img_atom[i]}', err_point[i], textcoords="offset points", xytext=(0,0), ha='center',color='black',fontsize=5) for i in range(len(err_data))]
               adjust_text(text)
            else:
                for i in range(len(err_data)):
                    plt.annotate(f'{err_img[i]}-{err_img_atom[i]}', err_point[i], textcoords="offset points", xytext=(0,0), ha='center',color='black',fontsize=5) 
        else:
            print('Invalid Err_str.txt file!')
        plt.savefig('Error.png', dpi=300)
    else:
        plt.savefig('Error.png', dpi=300)
        # plt.subplot(122)
        
        
def run(trajname, apps, option, fontsize, data=None, er_bar=1.5, out_type='traj', ra='Au', pot=None, cut_img=True, comment=False, text_adjust=False):
    traj = read(trajname, ':')
    if data is None:
        if pot is not None:
            data_normalization(apps, traj, 'images', pot=pot)
        else:
            raise FileNotFoundError('Please give a potential file! ')
    else:
        data_normalization(apps, traj, 'software', data=data)    
    if option == 'force':
        err_structure_finding(error_bar=er_bar, images=traj, otype=out_type, option=option, replace_atom=ra, Cutimg=cut_img, comment=comment, adjust=text_adjust, fontsize=fontsize)
    elif option == 'energy':
        err_structure_finding(error_bar=er_bar, images=traj, otype=out_type, option=option, replace_atom=ra, Cutimg=cut_img, comment=comment, adjust=text_adjust, fontsize=fontsize)