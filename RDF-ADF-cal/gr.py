'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-04-24 00:16:11
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-07-25 10:48:43
'''

import logging
import numpy as np
from ase.io import read
import matplotlib.pyplot as plt
import torch.distributed as dist
import os, time, re, itertools, math
from ase.neighborlist import neighbor_list
from torch.multiprocessing import Process
from scipy.interpolate import make_interp_spline

def init_process(rank, size, fn, traj, cutoff, bin_size, start, end, elements, rawDir, backend='gloo'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '12355'
    dist.init_process_group(backend, rank=rank, world_size=size)
    fn(rank, size, traj, cutoff, bin_size, start, end, elements, rawDir)

def genRDFRAW(trajname, cutoff, num_core, bin_size):
    print('=======================================================')
    print(
        'Starting a Radial Distribution Function job at %s',
        time.strftime('%X %x %Z'))
    images = read(trajname, ':')
    elements = []
    # Find all elements in the trajectory. 
    # The main reason for this setting is get away from the condition of different types of elements in different trajectories.
    for image in images:
        e_img = image.symbols.species()
        for e in e_img:
            if e not in elements:
                elements.append(e)
    nimages = len(images)
    cutoff = float(cutoff)
    print('  Total number of traning images: %d', nimages)
    print('=======================================================')
    total_of_cores = num_core
    processes = []
    num_each_process = math.ceil(nimages / total_of_cores)
    rawdir = os.path.dirname(trajname) + '/gr_data/raw_data'
    if not os.path.exists(rawdir):
        os.mkdir(rawdir)
    for rank in range(total_of_cores):
        start = num_each_process * rank
        end = start + num_each_process
        p = Process(target=init_process,
                    args=(rank, total_of_cores, raw_dataRDF, images[start:end], 
                          cutoff, bin_size, start, end, elements, rawdir))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()       


def RDFcalc(traj, cutoff, bin_size):
    '''
    NOTE: Only work when '/gr_data/raw_data' is created
    '''
    images = read(traj, ':')
    elements = []
    for image in images:
        e_img = image.symbols.species()
        for e in e_img:
            if e not in elements:
                elements.append(e)
    
    all_comb = list(itertools.product(elements, repeat=2))
    cut_comb = list({tuple(sorted(comb)) for comb in all_comb})
    for i in cut_comb:
        ele1 = i[0]
        ele2 = i[1]
        print("element list in rdf", ele1, ele2)
        mypath = os.path.dirname(traj) + '/gr_data'
        rdfdir = os.path.dirname(traj) + '/gr_data/raw_data'
        rdfsubdir = os.path.dirname(traj) + '/gr_data/RDF_ADF'
        if not os.path.exists(rdfsubdir):
            os.mkdir(rdfsubdir)
        if not os.path.exists(rdfdir):
            raise Exception('[info]   Have not call Gr_calc function')
        onlyfiles = [f for f in os.listdir(rdfdir) if os.path.isfile(os.path.join(rdfdir, f))]
        cutoff = float(cutoff)
        #cutoff = float(sys.argv[2])
        min_b = 0.1
        n_bins = int(cutoff / bin_size)
        t_bin, r = [], []
        search = 'TEST_' + str(ele1) + '_' + str(ele2)
        for fn in onlyfiles:
            if search in fn:
                f1 = open(rdfdir + '/' + fn, "r")
                lines = f1.readlines()
                if not r:
                    for line in lines:
                        temp = re.split(' |, |\n', line)
                        r.append(float(temp[0]))
                        t_bin.append(float(temp[2]))
                else:
                    j = 0
                    for line in lines:
                        temp = re.split(' |, |\n', line)
                        t_bin[j] += float(temp[2])
                        j += 1
                f1.close()
            
        tn_bin = [0 for i in range(0, n_bins)]
        bin1 = [min_b + k * bin_size for k in range(0, n_bins)]
        # make data beautiful and write final data for storage
        print('Writing cumulative data')
        filename = rdfsubdir + '/RDF_' + str(ele1) + '_' + str(ele2) + '_' + str(cutoff) + '.dat'
        
        f1 = open(filename, "w")
        for k in range(n_bins):
            r = min_b + k * bin_size
            # print(k)
            tn_bin[k] = t_bin[k] / (4 * 3.14 * r * r * bin_size)
            f1.write("%15.6f  %15.6f\n" % (r, tn_bin[k]))
        f1.close()
        X_Y_Spline = make_interp_spline(bin1, tn_bin)
        # find the max peak and write to fpParas.dat from tn_bin
        # we will have to store the data somewhere before writing it finally because
        # many different g(r) will report values
        points = []
        pos = []
        for i in range(1, len(tn_bin) - 1):
            if tn_bin[i - 1] < tn_bin[i] and tn_bin[i] > tn_bin[i + 1]:
                points.append(tn_bin[i])
                pos.append(min_b + i * bin_size)

        # write this to file and read in for all files
        filename = rdfsubdir + '/fp_' + str(ele1) + '_' + str(ele2) + '.dat'
        f1 = open(filename, "w")
        print(
            'Writing chosen fp data for this calculation to be used at the end'
        )
        for k in range(0, len(pos)):
            f1.write("%s %s %s %4.2f %4.2f %4.2f\n" %
                     ('G1', str(ele1), str(ele2), 20, pos[k], cutoff))
        f1.close()

        X_ = np.linspace(bin1[0], bin1[-1], 100)
        Y_ = X_Y_Spline(X_)

        first_y = max(tn_bin)
        first_x = bin1[tn_bin.index(first_y)]

        # plt.plot(bin1, t_bin, label="un-normalized Gr")
        plt.plot(bin1, tn_bin, label="normalized Gr")
        plt.plot(X_, Y_, label="smooth")
        plt.xlabel('distance R', fontsize=20)
        plt.ylabel('frequency', fontsize=20)
        plt.text(first_x, first_y, s="First peak: {:2f}".format(first_x))
        print(first_x)
        plt.legend()
        fname = rdfsubdir + '/RDF_' + str(ele1) + '_' + str(ele2) + '_' + str(
            cutoff) + '.png'
        plt.savefig(fname)
        plt.clf()
        # plt.show()
        # clean up files for now
        # for fn in onlyfiles:
        #     if search in fn:
        #         os.remove(rdfdir + '/' + fn)
        print('Closing the rdf job and deleted temporary dat files')
        
def raw_dataRDF(rank, size, traj, cutoff, bin_size, start, end, elements, rawDir):
    n_bins = int(cutoff/bin_size)
    min_b = 0.1
    e_list = elements
    
    all_comb = list(itertools.product(e_list, repeat=2))
    cut_comb = list({tuple(sorted(comb)) for comb in all_comb})
    for i in cut_comb:
        print('Starting RDF: {} \n'.format(i))
        n_bin = [0 for k in range(0, n_bins)]
        all_distance = []
        e1, e2 = i[0], i[1]
        t0 = time.time()
        m = start
        for img in traj:
            print('RDF Completed: %d of total %d in the %d to %d' %(m, len(img), start, end))
            m += 1
            cf_rdf = {(e1, e2): cutoff}
            nl = neighbor_list('d', img, cf_rdf)
            for d in nl:
                all_distance.append(d)
                for k in range(0, n_bins):
                    if min_b + k * bin_size <= d < min_b + (k + 1) * bin_size:
                        n_bin[k] += 1 # it goes over all neighbors twice already
                        break
        print("RDF time for one structure: ", time.time() - t0)
        
        tn_bin = [0 for i in range(0, n_bins)]
        filename = rawDir + '/TEST_' + str(e1) + '_' + str(e2) + '_' + str(start) + '_' + str(end) + '.dat'
        f1 = open(filename, "w")
        for k in range(0, n_bins):
            r = min_b + k*bin_size
            tn_bin[k] = n_bin[k] / (4 * np.pi * np.square(r) * bin_size)
            f1.write("%4.2f  %4.2f\n" %(r, n_bin[k]))
        f1.close()
        
if __name__ == '__main__':
    Dir = '/work/mse-wangjq'
    traj = Dir + '/train.traj'
    raw_save = Dir + '/gr_data'
    IFCALC = True
    # cutoff
    CUTOFF = 7
    # The size of each interval of RDF
    BIN_SIZE = 0.05
    # process number
    NUM_OF_CORE = 40
    if not os.path.exists(raw_save):
        os.mkdir(raw_save)
    if IFCALC:
        genRDFRAW(traj, CUTOFF, NUM_OF_CORE, BIN_SIZE)
        
    RDFcalc(traj, CUTOFF, BIN_SIZE)

    
    
    