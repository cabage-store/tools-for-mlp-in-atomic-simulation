'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-08-05 23:11:54
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-09-06 05:46:50
FilePath: /pyamff-wizard/wizard/Plottools/plotdensEF.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
import matplotlib
import numpy as np
import random, time
from ase.io import read
from pynep.calculate import NEP
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from wizard.io_generator import get_calc # type: ignore
from Plottools.plotfig import plot_Scatter
from ase.calculators.lammpslib import LAMMPSlib
# from pyamff.aseCalcF import aseCalcF


matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.direction'] = 'in' # in, out or inout
matplotlib.rcParams['ytick.right'] = False
matplotlib.rcParams['xtick.top'] = False

def get_DFT_NEP(filename, mode, potdir):
    imgs = read(filename, ':')
    if mode == 'NEP':
        calc = NEP(potdir)
    # elif mode == 'pyamff':
    #     calc = aseCalcF()
    # TODO: Add more calculator
    
    e_dft, e_mlp, f_dft, f_mlp = [], [], [], []
    for i in imgs:
        e_dft.append(i.get_potential_energy() / len(i))
        f_dft.append(i.get_forces().reshape(-1))
        i.set_calculator(calc)
        e_mlp.append(i.get_potential_energy() / len(i))
        f_mlp.append(i.get_forces().reshape(-1))

    e_dft = np.array(e_dft)
    e_mlp = np.array(e_mlp)
    f_dft = np.concatenate(f_dft)
    f_mlp = np.concatenate(f_mlp)
    return e_dft, e_mlp, f_dft, f_mlp
    
def pltForce(fT, fP, npltdata=100000):

    if len(fT) > npltdata:
        tmpidex = random.sample(range(len(fT)), npltdata)
        y_test = fT[tmpidex]
        y_prediction = fP[tmpidex]
    else:
        y_test = fT
        y_prediction = fP
    
    ### calculate gaussian_kde
    from scipy.stats import gaussian_kde
    #import matplotlib.cm as cm
    xy = np.vstack([y_test, y_prediction])
    z = gaussian_kde(xy)(xy)
    idex = np.lexsort([z])
    fig,ax=plt.subplots()

    # plt.title("NEP forces vs DFT forces", fontsize=16)
    ax.set_aspect(1)
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    
    ymajorFormatter = ticker.FormatStrFormatter('%.1f') 
    xmajorFormatter = ticker.FormatStrFormatter('%.1f') 
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    
    ax.spines['bottom'].set_linewidth(2)
    ax.spines['left'].set_linewidth(2)
    ax.spines['right'].set_linewidth(2)
    ax.spines['top'].set_linewidth(2)

    ax.tick_params(labelsize=14)

    lowLimit = np.min([np.min(y_test), np.min(y_prediction)])
    highLimit = np.max([np.max(y_test), np.max(y_prediction)])

    ax.plot([lowLimit, highLimit], [lowLimit, highLimit], 'k--', linewidth=1.0)
    scatter = ax.scatter(y_test[idex], y_prediction[idex], c=z[idex], cmap='Spectral_r',alpha=0.9, edgecolor='none', s=15)
    
    ax.set_xlim(np.min(y_test), np.max(y_test))
    ax.set_ylim(np.min(y_prediction), np.max(y_prediction))

    font3 = {'family':'sans-serif','size':16,'color':'k'}

    cbar = plt.colorbar(scatter,shrink=1,orientation='vertical',extend='both',pad=0.015,aspect=30)
    cbar.set_ticks([])
    cbar.update_ticks()
    cbar.set_label('Density',fontdict=font3)

    plt.xlabel("DFT Force (eV/Å)",fontdict=font3)
    plt.ylabel("NEP Force (eV/Å)",fontdict=font3)
    ax.axis('equal')
    ax.axis('square')

    rmse = np.sqrt(np.mean((y_test-y_prediction)**2))
    plt.text(np.mean(y_test), 
             np.min(y_prediction),
             "RMSE: {:.3f} eV/Å".format(rmse), fontsize=12)
    plt.savefig(f'DFTvsMLP-Force.png', dpi=300, bbox_inches='tight')
    

def pltEng(eT, eP, npltdata=100000):

    if len(eT) > npltdata:
        tmpidex = random.sample(range(len(eT)), npltdata)
        y_test = eT[tmpidex]
        y_prediction = eP[tmpidex]
    else:
        y_test = eT
        y_prediction = eP

    # calculate gaussian_kde
    from scipy.stats import gaussian_kde
    #import matplotlib.cm as cm
    xy = np.vstack([y_test, y_prediction])
    z = gaussian_kde(xy)(xy)
    idex = np.lexsort([z])
    fig,ax=plt.subplots()

    # plt.title("NEP energys vs DFT energy", fontsize=16)
    ax.set_aspect(1)
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    
    ymajorFormatter = ticker.FormatStrFormatter('%.2f') 
    xmajorFormatter = ticker.FormatStrFormatter('%.2f') 
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    
    ax.spines['bottom'].set_linewidth(2)
    ax.spines['left'].set_linewidth(2)
    ax.spines['right'].set_linewidth(2)
    ax.spines['top'].set_linewidth(2)

    ax.tick_params(labelsize=14)

    lowLimit = np.min([np.min(y_test), np.min(y_prediction)])
    highLimit = np.max([np.max(y_test), np.max(y_prediction)])

    ax.plot([lowLimit, highLimit], [lowLimit, highLimit], 'k--', linewidth=1.0)
    scatter = ax.scatter(y_test[idex], y_prediction[idex], c=z[idex], cmap='Spectral_r', alpha=0.9, edgecolor='none', s=30)
    
    ax.set_xlim(np.min(y_test), np.max(y_test))
    ax.set_ylim(np.min(y_prediction), np.max(y_prediction))

    font3 = {'family':'sans-serif','size':16,'color':'k'}
    cbar = plt.colorbar(scatter,shrink=1,orientation='vertical',extend='both',pad=0.015,aspect=30)
    cbar.set_ticks([])
    cbar.update_ticks()
    cbar.set_label('Density',fontdict=font3)
    
    plt.xlabel("DFT energy (eV/atom)",fontdict=font3)
    plt.ylabel("NEP energy (eV/atom)",fontdict=font3)
    ax.axis('equal')
    ax.axis('square')

    rmse = np.sqrt(np.mean((y_test-y_prediction)**2))
    plt.text(np.mean(y_test), 
             np.min(y_prediction),
             "RMSE: {:.3f} eV/atom".format(rmse), fontsize=12)
    plt.savefig(f'DFTvsMLP-Energy.png', dpi=300, bbox_inches='tight')
    

def plot_difP(traj, modes, potdir,
              title='DFT vs Different Potential Performance',
              labelx=['DFT Energy (eV/atom)','DFT Force (eV/Å)'],
              labely=['DifPot Energy (eV/atom)','DifPot Force (eV/Å)'],
              weight='normal', size=18, nfloat=[3,2], bins=[0.002, 1],
              color='b', linewidth=2, legend='line', marker='o', alpha=0.8,
              mlt=False, save_fig=True, savedir=['DFTvsDifP_E.png', 'DFTvsDifP_F.png']):
    imgs = read(traj, ':')
    allE = []
    allF = []
    for mid, mode in enumerate(modes): 
        energies, forces = [], []
        calc = None
        if mode == 'vasp':
            pass
        else:
            calc = get_calc(potdir[mid], mode)
        for i, image in enumerate(imgs):
            if mode == 'vasp':
                e = image.get_potential_energy() / len(image)
                f = image.get_forces().reshape(-1)
                energies.append(e)
                forces.append(f)
            else:
                image.set_calculator(calc)
                e = image.get_potential_energy() / len(image)
                f = image.get_forces().reshape(-1)
                energies.append(e)
                forces.append(f)      
            # elif mode == 'pyamff':
            #     calc = aseCalcF()
            # TODO: Add more calculator
        energies = np.array(energies).tolist()
        forces = np.concatenate(forces).tolist()
        allE.append(energies)
        allF.append(forces)
    t0 = time.time()
    print('[INFO]: Energy plot start')
    plot_Scatter([allE[0] for _ in range(len(modes)-1)], allE[1:], title, labelx[0], labely[0], 
                    weight=weight, size=size, nfloat=nfloat, bins=bins, mlunit='eV/atom', rmse=True, diagonal=True,
                    color=color, linewidth=linewidth, legend=legend, marker=marker, alpha=alpha,
                    mlt=mlt, save_fig=save_fig, savedir=savedir[0])
    t1 = time.time()
    print(f'[INFO]: Energy plot spend: {t1-t0}')
    print('[INFO]: Forces plot start')
    plot_Scatter([allF[0] for _ in range(len(modes)-1)], allF[1:], title, labelx[1], labely[1], 
                    weight=weight, size=size, nfloat=nfloat, bins=bins,mlunit='eV/Å', rmse=True, diagonal=True,
                    color=color, linewidth=linewidth, legend=legend, marker=marker, alpha=alpha,
                    mlt=mlt, save_fig=save_fig, savedir=savedir[1])
    t2 = time.time()
    print(f'[INFO]: Energy plot spend: {t2-t1}')
    print('Done!')