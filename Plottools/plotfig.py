'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-07-22 17:13:05
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2025-01-03 11:07:22
FilePath: /tools-for-mlp-in-atomic-simulation/Plottools/plotfig.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
import os
import random
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.ticker import MultipleLocator

matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.direction'] = 'in' # in, out or inout
matplotlib.rcParams['ytick.right'] = False
matplotlib.rcParams['xtick.top'] = False

def nice_color(mode, style):
    # TODO: Add more options
    styles = None
    
    if mode == 3:
        styles = [['#00A0FF', '#EF295A', '#FFAA00'], ['#501D8A', '#EE8C7D', '#aa3474']]
    
    return styles[style]
# Define font dict
def font_dict(family, size, color, weight):
    font3 = {'family':family,'size':size,'color':color, 'weight':weight}
    return font3

# Plot Polyline Figure
'''
weight: Font weight
size: fontsize
nfloat: The number of decimal places in x and y
bins: Axis tick interval
linecolor, linewidth : Color and width for line and tick line
savedir: Target dictonary to save figure.
'''
def plot_Polyline(x, y, title='Title', labelx='x label', labely='y label', 
                  weight='normal', size=18, nfloat=[1, 1], bins=None,
                  linecolor='b', linewidth=2, legend=None,
                  mlt=False, save_fig=False, savedir=os.getcwd()):
    fig, ax = plt.subplots()
    font3 = font_dict('Arial', size, 'black', weight)
    if mlt:
        for i in range(len(x)):
            ax.plot(x[i], y[i], color=linecolor[i], linewidth=linewidth)
    else:
        ax.plot(x, y, color=linecolor, linewidth=linewidth)

    # Set the aspect ratio
    # ax.set_aspect(1)
    
    # Setting major tick locators
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    if bins != None:
        # Set the major tick interval
        ax.xaxis.set_major_locator(MultipleLocator(bins[0])) 
        ax.yaxis.set_major_locator(MultipleLocator(bins[1]))
    else:
        pass
    # Set the major tick formatter
    if nfloat != None:
        xmajorFormatter = ticker.FormatStrFormatter(f'%.{nfloat[0]}f') 
        ymajorFormatter = ticker.FormatStrFormatter(f'%.{nfloat[1]}f')
    else:
        pass
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    # Set the line width of the axis border
    ax.spines['bottom'].set_linewidth(linewidth)
    ax.spines['left'].set_linewidth(linewidth)
    ax.spines['right'].set_linewidth(linewidth)
    ax.spines['top'].set_linewidth(linewidth)
    # Set the font size of the tick labels
    # Add a title and legend
    ax.tick_params(labelsize=size)
    for label in ax.get_xticklabels() + ax.get_yticklabels():
        label.set_fontweight(font3['weight'])
    ax.set_title(f"{title}", fontdict=font3)
    ax.set_xlabel(f"{labelx}", fontdict=font3)
    ax.set_ylabel(f"{labely}", fontdict=font3)
    if mlt:
        ax.legend(legend, prop={'weight':weight, 'size':size-2})
    else:
        ax.legend()

    if save_fig==True:
        plt.savefig(savedir, dpi=300, bbox_inches='tight')
    else:
        plt.show()

def plot_Scatter(x, y, title='Title', labelx='x label', labely='y label', mlunit=None, npltdata=100000,
                  weight='normal', size=18, nfloat=None, bins=None,
                  color='b', linewidth=2, legend=None, marker='o', alpha=0.8, rmse=False, diagonal=False,
                  mlt=False, save_fig=False, savedir=None):
    fig, ax = plt.subplots()
    font3 = font_dict('Arial', size, 'black', weight)
    xlow, ylow ,xhigh, yhigh = np.min(x), np.min(y), np.max(x), np.max(y)
    mlow = np.min([xlow, ylow])
    mhigh = np.max([xhigh, yhigh])
    if mlt:
        if len(x[0]) > npltdata:
            tmpidex = random.sample(range(len(x[0])), npltdata)
            x = x[tmpidex]
            for yi in y:
                yi = yi[tmpidex]
        else:
            x = x
            y = y
        for i in range(len(x)):
            ax.scatter(x[i], y[i], c=color[i], marker=marker, alpha=alpha)
    else:
        if len(x) > npltdata:
            tmpidex = random.sample(range(len(x[0])), npltdata)
            x = x[tmpidex]
            y = y[tmpidex]
        else:
            x = x
            y = y
        ax.scatter(x, y, c=color, marker=marker, alpha=alpha)
    if diagonal:
        ax.plot([mlow, mhigh], [mlow, mhigh], '--', c='black', linewidth=1)
    
    # Set the aspect ratio
    # ax.set_aspect(1)
    
    # Setting major tick locators
    xmajorLocator = ticker.MaxNLocator(5)
    ymajorLocator = ticker.MaxNLocator(5)
    ax.xaxis.set_major_locator(xmajorLocator)
    ax.yaxis.set_major_locator(ymajorLocator)
    # Set the major tick interval
    if bins is not None:
        ax.xaxis.set_major_locator(MultipleLocator(bins[0])) 
        ax.yaxis.set_major_locator(MultipleLocator(bins[1]))
    # Set the major tick formatter
    xmajorFormatter = ticker.FormatStrFormatter(f'%.{nfloat[0]}f') 
    ymajorFormatter = ticker.FormatStrFormatter(f'%.{nfloat[1]}f')
    ax.xaxis.set_major_formatter(xmajorFormatter)
    ax.yaxis.set_major_formatter(ymajorFormatter)
    # Set the line width of the axis border
    ax.spines['bottom'].set_linewidth(linewidth)
    ax.spines['left'].set_linewidth(linewidth)
    ax.spines['right'].set_linewidth(linewidth)
    ax.spines['top'].set_linewidth(linewidth)
    # Set the font size of the tick labels
    # Add a title and legend
    ax.tick_params(labelsize=size)
    for label in ax.get_xticklabels() + ax.get_yticklabels():
        label.set_fontweight(font3['weight'])
    ax.set_title(f"{title}", fontdict=font3)
    ax.set_xlabel(f"{labelx}", fontdict=font3)
    ax.set_ylabel(f"{labely}", fontdict=font3)
    if mlt:
        if legend is not None:
            ax.legend(legend, prop={'weight':weight, 'size':size-2})
    else:
        ax.legend()
    if rmse:
        if mlunit is not None:
            for i in range(len(x)):
                rmse_ = np.sqrt(np.mean(np.square(np.array(x)[i]-np.array(y)[i])))
                ax.text(xhigh, ylow+(yhigh-ylow)*0.05-(yhigh-ylow)*0.05*i*1, f"{legend[i]} RMSE: {rmse_:.3f} {mlunit}", ha='right', va='bottom', fontsize=12)
        else:
            raise ValueError('Please input right mlunit !!!!!')
            
    if save_fig==True:
        plt.savefig(savedir, dpi=300, bbox_inches='tight')
    else:
        plt.show()