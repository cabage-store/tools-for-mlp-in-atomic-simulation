'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-05-31 14:54:41
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-06-02 20:30:11
'''
import os
import glob
import numpy as np
from ase.io import read, write
from ase.visualize import view
from ase.build import cut, stack, sort

def bulk2surface(image, sur, layers=8):
    vector_dict = {'100':[[0, 1, 0], [0, 0, 1]], 
               '010':[[0, 0, 1], [1, 0, 0]], 
               '001':[[1, 0, 0], [0, 1, 0]], 
               '110':[[0, 0, 1], [1, -1, 0]], 
               '111':[[1, 0, -1], [0, 1, -1]],
               '211':[[1, -1, -1], [0, 1, -1]]}
    sur = str(sur)
    surface = cut(image, vector_dict[sur][0], vector_dict[sur][1], nlayers=layers)
    surface.center(vacuum=7.5, axis=2)
    return surface

def main(inputfp, same_name, sur_vector, nlayers, otype):
    global file_paths
    if same_name:
        file_paths = sorted(glob.glob(f'./**/{inputfp}', recursive=True))
        np.savetxt('file-queue.txt', file_paths, fmt='%s', delimiter="\t")
    elif same_name is None:
        file_paths = inputfp
    elif same_name is False:
        file_paths = sorted(glob.glob(f'./**/*.{inputfp}', recursive=True))
        np.savetxt('file-queue.txt', file_paths, fmt='%s', delimiter="\t")
    else:
        raise KeyError('Parameter same_name can only be True, False and None')
    
    if isinstance(file_paths, list):
        for i, f_path in enumerate(file_paths):
            try:
               img = read(f_path)
            except Exception as e:
                print(f'Error while reading {f_path}: {e}')
                continue
            if not os.path.exists(str(i)):
                os.mkdir(str(i))            
            if isinstance(sur_vector, list):
                for j in range(len(sur_vector)):
                    surface = bulk2surface(img, sur=sur_vector[j], layers=nlayers[j])
                    surface = sort(surface)
                    outname = f'{i}-{sur_vector[j]}.{otype}'
                    write(outname, surface, format=f'{otype}')
                    os.rename(outname, os.path.join(str(i), outname))
            else:
                surface = bulk2surface(img, sur=sur_vector, layers=nlayers)
                surface = sort(surface)
                outname = f'{i}-{sur_vector}.{otype}'
                write(outname, surface, format=f'{otype}')
                os.rename(outname, os.path.join(i, outname))

    else:
        try:
            img = read(file_paths)
        except Exception as e:
            print(f'Error while reading {file_paths}: {e}')
        if not os.path.exists('out'):
            os.mkdir('out')
        if isinstance(sur_vector, list):
            for j in sur_vector:
                surface = bulk2surface(img, sur=j)
                surface = sort(surface)
                outname = f'out-{j}.{otype}'
                write(outname, surface, format=f'{otype}')
                os.rename(outname, os.path.join('out', outname))
        else:
            surface = bulk2surface(img, sur=sur_vector)
            surface = sort(surface)
            outname = f'out-{j}.{otype}'
            write(outname, surface, format=f'{otype}')
            os.rename(outname, os.path.join('out', outname))
            
if __name__ == '__main__':
    main(inputfp='xsd', 
         same_name=False,
         sur_vector=['100','110','111','211'],
         nlayers=[16, 16, 8, 8],
         otype='vasp')
        
        