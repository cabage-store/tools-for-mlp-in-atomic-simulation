'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-07-22 03:36:21
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-07-31 14:43:42
FilePath: /tools-for-mlp-in-atomic-simulation/statistic_move_atoms/move.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-07-22 03:36:21
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-07-22 13:55:46
FilePath: /tools-for-mlp-in-atomic-simulation/statistic_move_atoms/move.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
import sys
import copy
import numpy as np
import matplotlib as mpl
from ase import Atoms, Atom
import matplotlib.pyplot as plt
from ase.io import read, Trajectory
from ase.neighborlist import NeighborList
from statistic_move_atoms.region import Region
from Converter.file_converter import get_symbols

def num_moveatoms(atoms, cutoff, region=None, mode='distance'):
    
    imgs = read(atoms, index=":") #input
    trajs = Trajectory('move_atom.traj', 'w') #output
        
    # Get id of image for different type of element.
    symbols = get_symbols(imgs[0])
    print(f"The symols of image is {symbols}")
    sym_num = len(symbols)
    
    coord_img = imgs[0].get_positions()
    target_id = Region(imgs[0], region)
    groups = [[imgs[0][at].index for at in target_id if imgs[0][at].symbol == symbols[i]] for i in range(sym_num)]
    # TODO: Optimize the group variable, use some fast method.
    

            
    # Set a empty box.
    img_t = Atoms()
    img_t.set_pbc([True, True, True])
    img_t.set_cell(imgs[0].get_cell())
    
    # Iteration of each trajectory.
    i=0
    for img in imgs: 
        j=0
        moved = []
        img_o = img_t.copy()
        img_n = img_t.copy()
        if i > 0:
            # last_img = img_t.copy()
            # present_img= img_t.copy()
            # for idx in target_id:
            #     last_img.append(imgs[i-1][idx])
            #     present_img.append(imgs[i][idx])
            for id in target_id:    
                if mode == 'distance':
                    # A temp image to compare the distance between present image atoms and last image atoms.
                    temp_img = img_t.copy()        
                    temp_img.append(img[id])
                    temp_img.append(imgs[i-1][id])  
                        
                    if temp_img.get_distance(0, 1, mic=True) > cutoff: # Dtmin: The minimum of torlerance shift distance.
                        j+=1
                        img_o.append(imgs[i-1][id])
                        img_n.append(img[id])
                        moved.append(id)
                elif mode == 'neighbor':
                    nl = NeighborList([cutoff]*len(imgs[0]), self_interaction=False, bothways=True)
                    nl.update(imgs[i-1])
                    index1, offsets1 = nl.get_neighbors(id)
                    nl.update(img)
                    index2, offsets2 = nl.get_neighbors(id)
                    if len(index1) != len(index2):
                        j+=1
                        img_o.append(imgs[i-1][id])
                        img_n.append(img[id])
                        moved.append(id)
                
            # After traversing all atoms, write img_o and img_n into the traj file
            trajs.write(img_o)
            trajs.write(img_n)

            num_pertype = {len(set(moved) & set(groups[i])):list(set(moved) & set(groups[i])) for i in range(len(groups))}
            # Output the trajectory i, the number of atoms j, and the ID of the moving atom
            print(num_pertype.keys())
            print(f'{i}\t{j}')
            # next structure
        i+=1
if __name__ == '__main__':
    # a = read('/Users/wx/Desktop/move_atoms/temp.xyz', ':')
    # num_moveatoms('/Users/wx/Desktop/move_atoms/temp.xyz', 1, region=[(13, 24), (0, a[0].get_cell()[1][1]), (0, a[0].get_cell()[2][2])], mode='distance')
    num_moveatoms('/Users/wx/Desktop/3_4.dump', 1, region=[(13, 24), (0, 'NF'), (0, 'NF')], mode='distance')