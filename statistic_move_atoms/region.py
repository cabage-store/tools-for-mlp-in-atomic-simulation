import numpy as np
from ase.io import read
def Region(img, region=None):
    
    target_id = range(len(img))
    coord_img = img.get_positions()
    
    if region is not None:
        region = np.array(region)
        if 'NF' in region:
            n_nf = np.argwhere(region =='NF')
            for n in n_nf:
                region[n[0]][n[1]] = img.cell.cellpar()[n[0]]
        else:
            pass
        region = region.astype(float)
        try:
            target_id = np.where((coord_img[:, 0] >= region[0][0]) & 
                                (coord_img[:, 0] <= region[0][1]) &
                                (coord_img[:, 1] >= region[1][0]) & 
                                (coord_img[:, 1] <= region[1][1]) &
                                (coord_img[:, 2] >= region[2][0]) & 
                                (coord_img[:, 2] <= region[2][1]))[0]
        except:
            raise ValueError(f'{region} must a [(xmin, xmax), (ymin, ymax), [zmin, zmax] type data structure.]')
    
    return target_id
        