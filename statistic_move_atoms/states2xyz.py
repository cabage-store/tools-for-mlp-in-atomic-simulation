'''
Description: 
version: 
Author: Alan
Date: 2022-12-27 22:00:15
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-07-19 06:36:43
'''
from ase.io import write, read
import argparse
import numpy as np
import pandas as pd
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

en_dict = {'H': 2.20,'He': 4.16,'Li': 0.98,'Be': 1.57,'B': 2.04,'C': 2.55,
            'N': 3.04,'O': 3.44,'F': 3.98,'Ne': 4.79,'Na': 0.93,'Mg': 1.31,
            'Al': 1.61,'Si': 1.98,'P': 2.19,'S': 2.58,'Cl': 3.16,'Ar': 3.24,
            'K': 0.82,'Ca': 1.01,'Sc': 1.36,'Ti': 1.54,'V': 1.63,'Cr': 1.66,
            'Mn': 1.55,'Fe': 1.83,'Co': 1.88,'Ni': 1.92,'Cu': 1.90,'Zn': 1.65,
            'Ga': 1.81,'Ge': 2.01,'As': 2.18,'Se': 2.55,'Br': 2.96,'Kr': 3.00,
            'Rb': 0.82,'Sr': 0.95,'Y': 1.22,'Zr': 1.33,'Nb': 1.59,'Mo': 2.16,
            'Tc': 1.91,'Ru': 2.20,'Rh': 2.28,'Pd': 2.20,'Ag': 1.93,'Cd': 1.69,
            'In': 1.78,'Sn': 1.96,'Sb': 2.05,'Te': 2.12,'I': 2.66,'Xe': 2.60,
            'Cs': 0.79,'Ba': 0.89,'Hf': 1.32,'Ta': 1.51,'W': 2.36,'Re': 1.93,
            'Os': 2.18,'Ir': 2.20,'Pt': 2.28,'Au': 2.54,'Hg': 2.00,'Ti': 1.62,
            'Pb': 2.33,'Bi': 2.02,'Po': 1.99,'At': 2.22,'Rn': 2.43
        }.keys()

en_dict = np.array(list(en_dict)*100)
en_dict = en_dict[en_dict != 'Cu']
en_dict = en_dict[en_dict != 'H']
en_dict = en_dict[en_dict != 'He']
en_dict = en_dict[en_dict != 'Zn']
en_dict = en_dict[en_dict != 'W']

process_names=['proc', 'saddle energy', 'prefactor', 'productID', 'product energy', 'product prefactor', 'barrier', 'rate', 'repeats']

def pbc(r, box, ibox = None):
    """
    Applies periodic boundary conditions.
    Parameters:
        r:      the vector the boundary conditions are applied to
        box:    the box that defines the boundary conditions
        ibox:   the inverse of the box. This will be calcluated if not provided.
    """
    if ibox is None:
        ibox = np.linalg.inv(box)
    vdir = np.dot(r, ibox)
    vdir = (vdir % 1.0 + 1.5) % 1.0 - 0.5
    return np.dot(vdir, box)

def per_atom_norm(v, box, ibox = None):
    '''
    Returns a length N np array containing per atom distance
        v:      an Nx3 np array
        box:    box matrix that defines the boundary conditions
        ibox:   the inverse of the box. will be calculated if not provided
    '''
    diff = pbc(v, box, ibox)
    return np.sqrt(np.sum(diff**2.0, axis=1))

def output_sub_event(state_path, sub_event_id, moved_cutoff, step):

    reactant = read(os.path.join(state_path, 'reactant_{}.con'.format(sub_event_id)))
    saddle = read(os.path.join(state_path, 'saddle_{}.con'.format(sub_event_id)))
    product = read(os.path.join(state_path, 'product_{}.con'.format(sub_event_id)))

    reactant.pbc = True
    saddle.pbc = True
    product.pbc = True

    box = reactant.cell
    ibox = np.linalg.inv(box)

    # define the atoms that moved more than moved_cutoff
    # pan1 = per_atom_norm(reactant.positions - saddle.positions, box, ibox)
    # pan2 = per_atom_norm(product.positions - saddle.positions, box, ibox)
    # moved_atoms_id1 = np.arange(len(pan1))[pan1 > moved_cutoff]
    # moved_atoms_id2 = np.arange(len(pan2))[pan2 > moved_cutoff]

    pan1 = per_atom_norm(reactant.positions - product.positions, box, ibox)
    moved_atoms_id1 = np.arange(len(pan1))[pan1 > moved_cutoff]

    # the overall moved atoms
    # moved_atoms = np.unique(np.concatenate([moved_atoms_id1, moved_atoms_id2]))
    moved_atoms = moved_atoms_id1 # it is now only consider diff between reactant and product, not consider saddle

    # moved atoms marked as other type of elements except 'Cu, H, He, Zn' 
    for i, id in enumerate(moved_atoms):
        reactant[id].symbol = en_dict[i]
        saddle[id].symbol = en_dict[i]
        product[id].symbol = en_dict[i]

    event_traj = [reactant, saddle, product]

    write(os.path.join(state_path, 'event_{}.extxyz'.format(sub_event_id)), event_traj)

    print('[output_sub_event] Step {}: state {} event {} done'.format(step, state_path, sub_event_id))

    return reactant, saddle, product


def output_events(path, dir_name, if_only_occured=True):

    state_dir = os.path.join(path, 'states')
    for state_id in dir_name:
        state_path = os.path.join(state_dir, str(state_id), 'procdata')
        process_table = pd.read_table(os.path.join(state_dir, str(state_id), 'processtable'), delimiter = r'\s+', skiprows=[0], names=process_names)
        # print(process_table)

        reactant_list = []
        saddle_list = []
        product_list = []
        for sub_event_id in range(len(process_table)):

            if if_only_occured and int(process_table[process_table.proc == sub_event_id].productID) == -1:
                continue

            reactant, saddle, product = output_sub_event(state_path, sub_event_id, MOVED_CUTOFF, state_id)
            reactant_list.append(reactant)
            saddle_list.append(saddle)
            product_list.append(product)
            
        # write(os.path.join(state_path, 'all_reactant.extxyz'), reactant_list)
        # write(os.path.join(state_path, 'all_saddle.extxyz'), saddle_list)
        # write(os.path.join(state_path, 'all_product.extxyz'), product_list)
    
    print('[INFO]   output_events done')



def traj_states(path, dir_name, max_step=0):
    # NOTE: states_xx.extxyz
    log_structures = list()
    for state_id in range(np.max(dir_name) + 1):
        curr_state = os.path.join(path, 'states', str(state_id))
        atoms = read(curr_state + '/reactant.con', index=0)
        atoms.pbc = True
        log_structures.append(atoms)
        print('[INFO]   state {} done'.format(state_id))

    write(path + '/states_{}.extxyz'.format(np.max(dir_name)), log_structures)

    # NOTE: time_dependent.extxyz
    if max_step > 0:
        print('starting time_dependent.extxyz')
        dynamics = pd.read_table(path + '/dynamics.txt',
                                    delimiter=r'\s+',
                                    skiprows=[0, 1],
                                    nrows=max_step,
                                    names=[
                                        'step-number', 'reactant-id', 'process-id',
                                        'product-id', 'step-time', 'total-time',
                                        'barrier', 'rate', 'energy'
                                    ])
        reac_id = dynamics['reactant-id'].to_numpy()
        proc_id = dynamics['process-id'].to_numpy()
        prod_id = dynamics['product-id'].to_numpy()

        time_structures = list()
        
        for i in range(len(dynamics)):

            state_path = path + '/states/{}/procdata'.format(reac_id[i])
            reac = read(os.path.join(state_path, 'reactant_{}.con'.format(proc_id[i])))
            reac.pbc = True
            time_structures.append(reac)

            print('[INFO]  traj_states time_step {} done'.format(i))
        
        prod = read(os.path.join(state_path, 'product_{}.con'.format(proc_id[i])))
        time_structures.append(prod)

    return time_structures


if __name__ == '__main__':
    # CONFIGURATION
    parser = argparse.ArgumentParser()
    parser.description=''
    parser.add_argument("-p", "--path", help="your default path", type=str, default=os.getcwd())
    parser.add_argument("-o", "--occurred", help="if only occurred",  type=str, default='True')
    parser.add_argument("-t", "--traj", help="if output time_dependent.extxyz",  type=str, default='False')
    args = parser.parse_args()

    if args.occurred == 'True':
        IF_ONLY_OCCURED = True
    elif args.occurred == 'False':
        IF_ONLY_OCCURED = False

    PATH = args.path

    MOVED_CUTOFF = 0.5

    dir_name = list()
    for i, j, k in os.walk(os.path.join(PATH, 'states')):
        dir_name = np.array(j, dtype=int)
        break
    dir_name = np.sort(dir_name)

    output_events(PATH, dir_name, if_only_occured=IF_ONLY_OCCURED)
    time_structures = traj_states(PATH, dir_name, max_step=20000)

    if args.traj == 'True':
        write(PATH + '/time_dependent.extxyz', time_structures)