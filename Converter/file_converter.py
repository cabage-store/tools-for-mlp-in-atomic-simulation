#! usr/bin/env python
#-*- coding utf-8 -*-
import os
import re
import glob
import numpy as np
from ase.build import sort
from ase.io import read, write
from ase.constraints import FixAtoms

def get_symbols(image, reverse=False):
    symbols_all = image.get_chemical_symbols()
    symbols_set = set(symbols_all)
    symbols = sorted(list(symbols_set), reverse=reverse)
    return symbols

def change_symbols(image, elements):
    symbols = get_symbols(image)
    for element, symbol in zip(elements, symbols):
        image.symbols[image.symbols.indices()[symbol]] = element
    return image

def onefile_converter(input_fname, convert_fname, ftype, lmpdat=False, need_constraint=False, sp_in_folder=False, trajfile=False):
    a = None
    elements = None
    if lmpdat:
        a = read(input_fname, ':')
        if os.path.exists('elements.txt'):
            elements = np.genfromtxt('elements.txt', dtype='str',delimiter='\n')

            if len(a) == 1:
                image = change_symbols(a[0], elements)
                a = image
            else:
                newimg = []
                for i in a:
                    image = change_symbols(i, elements)
                    newimg.append(image)
                a = newimg
        else:
            print('No elements.txt file, elements will be set as lammps default.')
    else:
        a = read(input_fname, ':')
    if len(a) == 1:
        a = a[0]
        if need_constraint:
            try:
                fix_id = np.loadtxt('fixatom.txt')
                print('Load fixatom.txt.')
            except Exception as e:
                print("An error occured:", e)
            b = FixAtoms(indices=fix_id)
            a.set_constraint(b)
        else:
            pass

        if ftype == 'vasp': # if not sort the POSCAR, OUTCAR will can't be read.
            if input_fname ==( 'POSCAR' or 'CONTCAR' or 'OUTCAR'):
                write('POSCAR', a, format=ftype)
                nimg = read('POSCAR')
                accuracy = 30
                lc = nimg.cell.cellpar()
                k = np.ceil(accuracy / lc) .astype(int)
                os.system(f'gkpoints {k[0]} {k[1]} {k[2]}')
            else:
                sort_img = sort(a)
                write('POSCAR', sort_img, format=ftype)
                nimg = read('POSCAR')
                accuracy = 30
                lc = nimg.cell.cellpar()
                k = np.ceil(accuracy / lc) .astype(int)
                os.system(f'gkpoints {k[0]} {k[1]} {k[2]}')
                
        elif ftype == 'lammps-data':
            write(f'{convert_fname}.data', a, format=ftype)
        elif ftype == 'extxyz':
            write(f'{convert_fname}.xyz', a, format=ftype)
        else:
            write(f'{convert_fname}.{ftype}', a, format=ftype)
    else:
        traj_range = None
        if os.path.exists('frames-range.txt'):
            try:
                sp_traj = np.loadtxt('frames-range.txt', ndmin=2)
            except Exception as e:
                print("An error occured:", e)
            traj_range = list(map(lambda x : slice(x[0], x[1]+1),  sp_traj.astype(int)))
                
        elif os.path.exists('frames.txt'):
            try:
                sp_traj = np.loadtxt('frames.txt',dtype=int, ndmin=1).reshape(-1)
            except Exception as e:
                print("An error occured:", e)
            traj_range = sp_traj
        else:
            traj_range = range(len(a))
            print('No frames.txt or frames-range.txt file, all of trajectories will be generated.')
                        
        if not trajfile:        
            if not os.path.exists(convert_fname):
                os.makedirs(convert_fname)
            for i in traj_range:
                if sp_in_folder:
                    fn = None
                    fp = f'{i}'
                    if not os.path.exists(fp):
                        os.makedirs(fp)
                    if ftype == 'vasp':
                        fn = 'POSCAR'
                        sort_img = sort(a[i])
                        write(fn, sort_img, format=ftype)
                    elif ftype == 'lammps-data':
                        fn = f'{i}lmp.data'
                        write(fn, a[i], format=ftype)
                    elif ftype == 'extxyz':
                        fn = f'{i}.xyz'
                        write(fn, a[i], format=ftype)
                    else:
                        fn = f'{i}.{ftype}'
                        write(fn, a[i], format=ftype)
                    os.rename(fn, os.path.join(fp, fn))
                    os.rename(fp, os.path.join(convert_fname, fp))
                else:
                    fn = None
                    if ftype == 'vasp':
                        fn = f'{i}.vasp'
                        sort_img = sort(a[i])
                        write(fn, sort_img, format=ftype)
                    elif ftype == 'lammps-data':
                        fn = f'{i}lmp.data'
                        write(fn, a[i], format=ftype)
                    elif ftype == 'extxyz':
                        fn = f'{i}.xyz'
                        write(fn, a[i], format=ftype)
                    else:
                        fn = f'{i}.{ftype}'
                        write(fn, a[i], format=ftype)
                    os.rename(fn, os.path.join(convert_fname, fn))
        else:
            if os.path.exists('frames.txt'):
                img = [a[i] for i in traj_range]                
                if ftype == 'extxyz':
                    write(f'{convert_fname}.xyz', img, format=ftype)
                else:
                    write(f'{convert_fname}.{ftype}', img, format=ftype)
            elif os.path.exists('frames-range.txt'):
                img = a[traj_range[0]]
                if ftype == 'extxyz':
                    write(f'{convert_fname}.xyz', img, format=ftype)
                else:
                    write(f'{convert_fname}.{ftype}', img, format=ftype)
            else:
                if ftype == 'extxyz':
                    write(f'{convert_fname}.xyz', a, format=ftype)
                else:
                    write(f'{convert_fname}.{ftype}', a, format=ftype)                
    print("All done!")


def mltfile_converter(inputfp=None, ini_type=None, out_name=None, ftype=None, same_name=True, append=True, need_constraint=False, lmpdat=False):
    if same_name:
        file_paths = glob.glob(f'./**/{inputfp}', recursive=True)
        file_paths.sort(key = lambda x: int(re.findall(r'\d+', x)[0]) if re.findall(r'\d+', x) else float('inf'))
        np.savetxt('file-queue.txt', file_paths, fmt='%s', delimiter="\t")
        if not os.path.exists(out_name):
            os.makedirs(out_name)
        for i, f_path in enumerate(file_paths):
            try:
                if lmpdat:
                    img = read(f_path, ':')
                    if os.path.exists('elements.txt'):
                        elements = np.genfromtxt('elements.txt', dtype='str',delimiter='\n')
                        newimg = []
                        for ii in img:
                            image = change_symbols(ii, elements)
                            newimg.append(image)
                        img = newimg
                    else:
                        print('No elements.txt file, elements will be set as lammps default.')
                else:
                    img = read(f_path, ':')
                    
            except Exception as e:
                print(f'Error while reading {f_path}: {e}')
                continue
                        
            if append:
                write(f'{out_name}.{ftype}', img, append=append, format=ftype)
            else:
                if need_constraint:
                    try:
                        fix_id = np.loadtxt('fixatom.txt')
                        print('Load fixatom.txt.')
                    except Exception as e:
                        print("An error occured:", e)
                    b = FixAtoms(indices=fix_id)
                    for j in range(len(img)):
                        img[j].set_constraint(b)
                        img[j] = sort(img[j])
                else:
                    for j in range(len(img)):
                        img[j] = sort(img[j])
                write(f'{i}.{ftype}', img, format=ftype)
                os.rename(f'{i}.{ftype}', os.path.join(out_name, f'{i}.{ftype}'))
        if os.path.exists(f'{out_name}.{ftype}'):
            os.rename(f'{out_name}.{ftype}', os.path.join(out_name, f'{out_name}.{ftype}'))
            
    else:
        file_paths = glob.glob(f'./**/*.{ini_type}', recursive=True)
        file_paths.sort(key = lambda x: int(re.findall(r'\d+', x)[0]) if re.findall(r'\d+', x) else float('inf'))
        np.savetxt('file-queue.txt', file_paths, fmt='%s', delimiter="\t")
        if not os.path.exists(out_name):
            os.makedirs(out_name)
        for i, f_path in enumerate(file_paths):
            try:
                if lmpdat:
                    img = read(f_path, ':')
                    if os.path.exists('elements.txt'):
                        elements = np.genfromtxt('elements.txt', dtype='str',delimiter='\n')
                        newimg = []
                        for ii in img:
                            image = change_symbols(ii, elements)
                            newimg.append(image)
                        img = newimg
                    else:
                        print('No elements.txt file, elements will be set as lammps default.')
                else:
                    img = read(f_path, ':')
                    
            except Exception as e:
                print(f'Error while reading {f_path}: {e}')
                continue
            if append:
                write(f'{out_name}.{ftype}', img, append=append, format=ftype)
            else:
                if need_constraint:
                    try:
                        fix_id = np.loadtxt('fixatom.txt')
                        print('Load fixatom.txt.')
                    except Exception as e:
                        print("An error occured:", e)
                    b = FixAtoms(indices=fix_id)
                    for j in range(len(img)):
                        img[j].set_constraint(b)
                        img[j] = sort(img[j])
                else:
                    for j in range(len(img)):
                        img[j] = sort(img[j])
                        
                write(f'{i}.{ftype}', img, format=ftype)
                os.rename(f'{i}.{ftype}', os.path.join(out_name, f'{i}.{ftype}'))
        if os.path.exists(f'{out_name}.{ftype}'):
            os.rename(f'{out_name}.{ftype}', os.path.join(out_name, f'{out_name}.{ftype}'))
    print('All done!')
