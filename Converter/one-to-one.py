'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-04-22 21:14:09
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-06-03 10:00:41
FilePath: /tools-for-mlp-in-atomic-simulation/Converter/one-to-one.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
import sys
from file_converter import onefile_converter

v = sys.argv
onefile_converter(input_fname=v[1], convert_fname=v[2], ftype=v[3], need_constraint=bool(int(v[4])),trajfile=bool(int(v[5])), lmpdat=bool(int(v[6]))) 
