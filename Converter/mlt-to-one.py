'''
Author: glab-cabage 2227541807@qq.com
Date: 2024-04-22 21:14:09
LastEditors: glab-cabage 2227541807@qq.com
LastEditTime: 2024-06-03 10:31:25
FilePath: /tools-for-mlp-in-atomic-simulation/Converter/mlt-to-one.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
import sys
from file_converter import mltfile_converter

v = sys.argv
if bool(int(v[4])):
    mltfile_converter(inputfp=v[1], out_name=v[2], ftype=v[3], same_name=bool(int(v[4])), append=True, lmpdat=bool(int(v[5])))
else:
    mltfile_converter(ini_type=v[1], out_name=v[2], ftype=v[3], same_name=False, append=True, lmpdat=bool(int(v[5])))
