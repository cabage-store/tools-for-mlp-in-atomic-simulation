import sys
from file_converter import onefile_converter

v = sys.argv
lmpdata = bool(int(v[1]))
if lmpdata:
    onefile_converter(lmpdat=True, input_fname=v[2], convert_fname=v[3], ftype=v[4], sp_in_folder=bool(int(v[5])))
else:
    onefile_converter(input_fname=v[2], convert_fname=v[3], ftype=v[4], sp_in_folder=bool(int(v[5])))
